-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table pneumoniasugeno.aturans: ~21 rows (approximately)
/*!40000 ALTER TABLE `aturans` DISABLE KEYS */;
INSERT INTO `aturans` (`id_aturan`, `nama_aturan`, `suhu`, `nadi`, `pernafasan`, `usia`, `pao2`, `sistolik`, `ph`, `bun`, `natrium`, `glukosa`, `hematokrit`, `efusi_pleura`, `keganasan`, `penyakit_hati`, `jantung`, `serebrovaskular`, `ginjal`, `gangguan_kesadaran`, `pneumonia`, `remember_token`, `created_at`, `updated_at`) VALUES
	(4, 'R1', 'rendah', 'tinggi', 'cepat', 'lansia', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 275.00, NULL, '2018-05-20 09:01:10', '2018-05-20 09:01:10'),
	(5, 'R2', 'panas', 'tinggi', 'cepat', 'lansia', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 275.00, NULL, '2018-05-20 09:04:30', '2018-05-20 09:04:30'),
	(6, 'R3', 'rendah', 'tinggi', 'cepat', 'dewasa', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Ya', 'Ya', 'Ya', 'Tidak', 'Tidak', 'Tidak', 215.00, NULL, '2018-05-20 09:06:40', '2018-05-20 09:06:40'),
	(7, 'R4', 'panas', 'tinggi', 'cepat', 'muda', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Ya', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 215.00, NULL, '2018-05-20 09:18:49', '2018-05-20 09:18:49'),
	(8, 'R5', 'rendah', 'tinggi', 'cepat', 'dewasa', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Tidak', 'Tidak', 'Ya', 'Tidak', 'Tidak', 'Tidak', 175.00, NULL, '2018-05-20 09:20:58', '2018-05-20 09:20:58'),
	(9, 'R6', 'rendah', 'tinggi', 'cepat', 'lansia', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Ya', 'Tidak', 'Tidak', 185.00, NULL, '2018-05-20 09:24:46', '2018-05-20 09:24:46'),
	(10, 'R7', 'panas', 'tinggi', 'cepat', 'lansia', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Ya', 'Ya', 205.00, NULL, '2018-05-20 10:27:35', '2018-05-20 10:27:35'),
	(11, 'R8', 'rendah', 'tinggi', 'cepat', 'muda', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 175.00, NULL, '2018-05-20 10:30:24', '2018-05-20 10:30:24'),
	(12, 'R9', 'panas', 'tinggi', 'cepat', 'dewasa', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Tidak', 'Tidak', 'Ya', 'Tidak', 'Ya', 'Ya', 205.00, NULL, '2018-05-20 10:33:14', '2018-05-20 10:33:14'),
	(13, 'R10', 'panas', 'tinggi', 'cepat', 'dewasa', 'hipoksia', 'rendah', 'asam', 'tinggi', 'rendah', 'tinggi', 'tinggi', 'Ya', 'Tidak', 'Tidak', 'Ya', 'Tidak', 'Ya', 'Ya', 205.00, NULL, '2018-05-20 10:35:23', '2018-05-20 10:35:23'),
	(14, 'R11', 'normal', 'normal', 'normal', 'lansia', 'normal', 'tinggi', 'normal', 'normal', 'normal', 'tinggi', 'normal', 'Ya', 'Tidak', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 50.00, NULL, '2018-05-20 10:37:29', '2018-05-20 10:37:29'),
	(15, 'R12', 'normal', 'normal', 'normal', 'lansia', 'normal', 'rendah', 'normal', 'normal', 'normal', 'tinggi', 'normal', 'Tidak', 'Tidak', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 60.00, NULL, '2018-05-20 12:17:54', '2018-05-20 12:17:54'),
	(16, 'R13', 'normal', 'normal', 'cepat', 'muda', 'normal', 'rendah', 'normal', 'normal', 'normal', 'tinggi', 'normal', 'Ya', 'Tidak', 'Tidak', 'Ya', 'Tidak', 'Tidak', 'Tidak', 60.00, NULL, '2018-05-20 12:19:59', '2018-05-20 12:19:59'),
	(17, 'R14', 'normal', 'normal', 'cepat', 'lansia', 'normal', 'rendah', 'normal', 'normal', 'normal', 'tinggi', 'normal', 'Tidak', 'Tidak', 'Ya', 'Ya', 'Tidak', 'Tidak', 'Tidak', 70.00, NULL, '2018-05-20 12:22:31', '2018-05-20 12:22:31'),
	(18, 'R15', 'panas', 'normal', 'cepat', 'dewasa', 'normal', 'normal', 'normal', 'tinggi', 'tinggi', 'tinggi', 'tinggi', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Ya', 'Tidak', 85.00, NULL, '2018-05-20 12:25:34', '2018-05-20 12:25:34'),
	(19, 'R16', 'normal', 'tinggi', 'cepat', 'muda', 'hipoksia', 'normal', 'normal', 'normal', 'normal', 'tinggi', 'rendah', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 70.00, NULL, '2018-05-20 12:29:46', '2018-05-20 12:29:46'),
	(20, 'R17', 'rendah', 'tinggi', 'cepat', 'dewasa', 'normal', 'rendah', 'normal', 'normal', 'normal', 'normal', 'normal', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Ya', 85.00, NULL, '2018-05-20 12:33:58', '2018-05-20 12:33:58'),
	(21, 'R18', 'rendah', 'tinggi', 'cepat', 'lansia', 'normal', 'rendah', 'normal', 'normal', 'normal', 'normal', 'normal', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Ya', 95.00, NULL, '2018-05-20 12:45:36', '2018-05-20 12:45:36'),
	(22, 'R19', 'rendah', 'tinggi', 'cepat', 'muda', 'normal', 'rendah', 'normal', 'normal', 'normal', 'normal', 'normal', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Ya', 65.00, NULL, '2018-05-20 12:47:38', '2018-05-20 12:47:38'),
	(23, 'R20', 'normal', 'normal', 'normal', 'dewasa', 'normal', 'normal', 'asam', 'normal', 'normal', 'normal', 'rendah', 'Ya', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Ya', 100.00, NULL, '2018-05-20 12:51:19', '2018-05-20 12:51:19'),
	(24, 'R21', 'normal', 'tinggi', 'normal', 'lansia', 'normal', 'normal', 'asam', 'normal', 'normal', 'normal', 'normal', 'Tidak', 'Tidak', 'Ya', 'Tidak', 'Tidak', 'Ya', 'Ya', 100.00, NULL, '2018-05-20 12:53:53', '2018-05-20 12:53:53');
/*!40000 ALTER TABLE `aturans` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.daftar__variabels: ~18 rows (approximately)
/*!40000 ALTER TABLE `daftar__variabels` DISABLE KEYS */;
INSERT INTO `daftar__variabels` (`id_variable`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 'suhu', '2018-05-16 18:21:22', '2018-05-16 18:21:23'),
	(2, 'denyut nadi', '2018-05-21 18:36:15', '2018-05-21 18:36:16'),
	(3, 'pernafasan', '2018-05-21 18:36:32', '2018-05-21 18:36:33'),
	(4, 'usia', '2018-05-21 18:36:53', '2018-05-21 18:36:54'),
	(5, 'Pao2', '2018-05-21 18:37:14', '2018-05-21 18:37:14'),
	(6, 'Sistolik', '2018-05-21 18:37:28', '2018-05-21 18:37:29'),
	(7, 'Ph', '2018-05-21 18:37:42', '2018-05-21 18:37:43'),
	(8, 'BUN', '2018-05-21 18:38:01', '2018-05-21 18:37:47'),
	(9, 'Natrium', '2018-05-21 18:38:28', '2018-05-21 18:38:30'),
	(10, 'Glukosa', '2018-05-21 18:38:53', '2018-05-21 18:38:53'),
	(11, 'Hematokrit', '2018-05-21 18:39:05', '2018-05-21 18:39:05'),
	(12, 'Efusi Pleura', '2018-05-21 18:39:20', '2018-05-21 18:39:21'),
	(13, 'Keganasan Komorbid', '2018-05-21 18:39:40', '2018-05-21 18:39:41'),
	(14, 'Riwayat Penyakit Hati', '2018-05-21 18:39:55', '2018-05-21 18:39:56'),
	(15, 'Riwayat Penyakit Jantung', '2018-05-21 18:39:55', '2018-05-21 18:39:56'),
	(16, 'Riwayat Serebrovaskular', '2018-05-21 18:40:52', '2017-05-21 18:40:53'),
	(17, 'Riwayat Penyakit Ginjal', '2018-05-21 18:41:28', '2018-05-21 18:41:28'),
	(18, 'Riwayat Gangguan kesadaran', '2018-05-21 18:41:53', '2018-05-21 18:41:53');
/*!40000 ALTER TABLE `daftar__variabels` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.gejalas: ~1 rows (approximately)
/*!40000 ALTER TABLE `gejalas` DISABLE KEYS */;
INSERT INTO `gejalas` (`id_gejala`, `id_pasien`, `suhu`, `nadi`, `pernafasan`, `usia`, `pao2`, `sistolik`, `ph`, `bun`, `natrium`, `glukosa`, `hematokrit`, `efusi_pleura`, `keganasan`, `penyakit_hati`, `jantung`, `serebrovaskular`, `ginjal`, `gangguan_kesadaran`, `nilai_z`, `pneumonia`, `solusi`, `remember_token`, `created_at`, `updated_at`) VALUES
	(8, 5, 36, 120, 15, '57', 105, 125, 7.31, 20, 132, 130, 49, 0, 0, 1, 0, 0, 1, 1, 100, 'pneumonia sedang', 'rawat Inap', NULL, '2018-05-29 09:12:23', '2018-05-29 09:12:23');
/*!40000 ALTER TABLE `gejalas` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.himpunans: ~45 rows (approximately)
/*!40000 ALTER TABLE `himpunans` DISABLE KEYS */;
INSERT INTO `himpunans` (`id_himpunan`, `id_variable`, `nama_himpunan`, `batasbawah`, `batastengah_a`, `batastengah_b`, `batasatas`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Suhu Rendah', 35, NULL, NULL, 36.5, '2018-05-21 18:44:19', '2018-05-21 18:44:20'),
	(2, 1, 'Suhu Normal', 35, 36.5, 37.2, 40, '2018-05-21 18:45:41', '2018-05-21 18:45:41'),
	(3, 1, 'Suhu Panas', 37.2, NULL, NULL, 40, '2018-05-21 18:46:37', '2018-05-21 18:46:39'),
	(4, 2, 'Nadi Rendah', 50, NULL, NULL, 60, '2018-05-21 18:48:19', '2018-05-21 18:48:19'),
	(5, 2, 'Nadi Normal', 50, 60, 100, 125, '2018-05-21 18:49:30', '2018-05-21 18:49:30'),
	(6, 2, 'Nadi Tinggi', 100, NULL, NULL, 125, '2018-05-21 18:49:54', '2018-05-21 18:50:03'),
	(7, 3, 'Pernafasan Lemah', 14, NULL, NULL, 16, '2018-05-21 18:50:41', '2018-05-21 18:50:42'),
	(8, 3, 'Pernafasan Normal', 14, 16, 20, 30, '2018-05-21 18:54:02', '2018-05-21 18:54:02'),
	(9, 3, 'Pernafasan Cepat', 20, NULL, NULL, 30, '2018-05-21 18:54:30', '2018-05-21 18:54:31'),
	(10, 4, 'Usia Muda', 25, NULL, NULL, 30, '2018-05-21 18:55:55', '2018-05-21 18:55:56'),
	(11, 4, 'Usia Dewasa', 25, 30, 50, 56, '2018-05-21 18:56:39', '2018-05-21 18:56:40'),
	(12, 4, 'Usia Lansia', 50, NULL, NULL, 56, '2018-05-21 18:57:09', '2018-05-21 18:57:10'),
	(13, 5, 'Pao2 Hipoksia', 60, NULL, NULL, 80, '2018-05-21 18:58:23', '2018-05-21 18:58:24'),
	(14, 5, 'Pao2 Normal', 60, 80, 100, 110, '2018-05-21 18:58:57', '2018-05-21 18:58:58'),
	(15, 6, 'Sistolik Rendah', 90, NULL, NULL, 110, '2018-05-21 18:59:40', '2018-05-21 18:59:41'),
	(16, 6, 'Sistolik Normal', 90, 110, 120, 140, '2018-05-21 19:00:39', '2018-05-21 19:00:39'),
	(17, 6, 'Sistolik Tinggi', 120, NULL, NULL, 140, '2018-05-21 19:01:08', '2018-05-21 19:01:09'),
	(18, 7, 'Ph Asam', 7.3, NULL, NULL, 7.35, '2018-05-21 19:01:47', '2018-05-21 19:01:48'),
	(19, 7, 'Ph Normal', 7.3, 7.35, 7.45, 7.5, '2018-05-21 19:02:37', '2018-05-21 19:02:37'),
	(20, 7, 'Ph Basa', 7.45, NULL, NULL, 7.5, '2018-05-21 19:03:23', '2018-05-21 19:03:25'),
	(22, 8, 'BUN Normal', 0, NULL, NULL, 26, '2015-05-21 19:04:27', '2018-05-29 04:56:49'),
	(23, 8, 'BUN Tinggi', 24, NULL, NULL, 200, '2018-05-21 19:05:02', '2018-05-21 19:05:02'),
	(24, 9, 'Natrium Rendah', 130, NULL, NULL, 135, '2018-05-21 19:05:42', '2018-05-21 19:05:42'),
	(25, 9, 'Natrium Normal', 130, 135, 140, 145, '2018-05-21 19:06:29', '2018-05-21 19:06:30'),
	(26, 9, 'Natrium Tinggi', 140, NULL, NULL, 145, '2018-05-21 19:06:57', '2018-05-21 19:06:57'),
	(27, 10, 'Glukosa Rendah', 70, NULL, NULL, 80, '2018-05-21 19:07:31', '2018-05-21 19:07:32'),
	(28, 10, 'Glukosa Normal', 70, 80, 120, 250, '2018-05-21 19:08:03', '2018-05-21 19:08:04'),
	(29, 10, 'Glukosa Tinggi', 120, NULL, NULL, 250, '2018-05-21 19:08:30', '2018-05-21 19:08:30'),
	(30, 11, 'Hematokrit Rendah', 34, NULL, NULL, 37, '2018-05-21 19:09:12', '2018-05-21 19:09:12'),
	(31, 11, 'Hematokrit Normal', 34, 37, 48, 50, '2018-05-21 19:09:44', '2018-05-21 19:09:45'),
	(32, 11, 'Hematokrit Tinggi', 48, NULL, NULL, 50, '2018-05-21 19:10:04', '2018-05-21 19:10:05'),
	(33, 12, 'Efusi Pleura Ya', 0, NULL, NULL, 1, '2018-05-21 19:11:41', '2018-05-21 19:11:41'),
	(34, 12, 'Efusi Pleura Tidak', 0, NULL, NULL, 1, '2018-05-21 19:12:11', '2018-05-21 19:12:11'),
	(35, 13, 'Keganasan Ya', 0, NULL, NULL, 1, '2018-05-21 19:12:48', '2018-05-21 19:12:48'),
	(36, 13, 'Keganasan Tidak', 0, NULL, NULL, 1, '2018-05-21 19:13:11', '2018-05-21 19:13:12'),
	(37, 14, 'Penyakit Hati Ya', 0, NULL, NULL, 1, '2018-05-21 19:13:39', '2018-05-21 19:13:39'),
	(38, 14, 'Penyakit Hati Tidak', 0, NULL, NULL, 1, '2018-05-21 19:13:57', '2018-05-21 19:13:57'),
	(39, 15, 'Penyakit Jantung Ya', 0, NULL, NULL, 1, '2018-05-21 19:14:14', '2018-05-21 19:14:14'),
	(40, 15, 'Penyakit Jantung Tidak', 0, NULL, NULL, 1, '2018-05-21 19:14:52', '2018-05-21 19:14:53'),
	(41, 16, 'Serebrovaskular Ya', 0, NULL, NULL, 1, '2018-05-21 19:15:19', '2018-05-21 19:15:20'),
	(42, 16, 'Serebrovaskular Tidak', 0, NULL, NULL, 1, '2018-05-21 19:15:50', '2018-05-21 19:15:51'),
	(43, 17, 'Penyakit Ginjal Ya', 0, NULL, NULL, 1, '2018-05-21 19:16:09', '2018-05-21 19:16:09'),
	(44, 17, 'Penyakit Ginjal Tidak', 0, NULL, NULL, 1, '2018-05-21 19:16:32', '2018-05-21 19:16:33'),
	(45, 18, 'Gangguan Kesadaran Ya', 0, NULL, NULL, 1, '2018-05-21 19:16:54', '2018-05-21 19:16:54'),
	(46, 18, 'Gangguan Kesadaran Tidak', 0, NULL, NULL, 1, '2018-05-21 19:17:12', '2018-05-21 19:17:13');
/*!40000 ALTER TABLE `himpunans` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.migrations: ~8 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_05_08_121002_create_pasiens_table', 1),
	(4, '2018_05_08_121152_create_daftar__variabels_table', 1),
	(5, '2018_05_08_121230_create_solusis_table', 1),
	(6, '2018_05_08_121315_create_aturans_table', 1),
	(7, '2018_05_08_182638_create_gejalas_table', 1),
	(8, '2018_05_08_184818_create_himpunans_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.pasiens: ~2 rows (approximately)
/*!40000 ALTER TABLE `pasiens` DISABLE KEYS */;
INSERT INTO `pasiens` (`id_pasien`, `nama`, `nama_panggilan`, `alamat`, `no_telepon`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `status_perkawinan`, `kewarganegaraan`, `agama`, `pekerjaan`, `email`, `pendidikan`, `nama_kerabat`, `hubungan`, `nomor_telepon`, `remember_token`, `created_at`, `updated_at`) VALUES
	(4, 'ahmad syahriza', 'riza', 'balikpapan', '0857531242367', 'balikpapan', '14-02-1995', 'laki', 'menikah', 'indonesia', 'islam', 'mahasiswa', 'rija@gmail.com', 'sarjana', 'sulaiman ahda', 'ayah', '085743124236', NULL, '2018-05-20 13:39:15', '2018-05-20 13:45:50'),
	(5, 'siddiq', 'siddiq', 'gpw', '085312341234', 'banjarmasin', '12-06-1960', 'laki', 'lajang', 'indonesia', 'islam', 'mahasiswa', 'sid.diq@gmail.com', 'S1', 'rija', 'kerabat', '085123123123', NULL, '2018-05-29 06:25:18', '2018-05-29 06:25:18');
/*!40000 ALTER TABLE `pasiens` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.solusis: ~2 rows (approximately)
/*!40000 ALTER TABLE `solusis` DISABLE KEYS */;
INSERT INTO `solusis` (`id_solusi`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 'rawat Inap', '2018-05-16 17:37:07', '2018-05-16 11:20:26'),
	(2, 'rawat jalan', '2018-05-16 18:02:05', '2018-05-16 18:02:06');
/*!40000 ALTER TABLE `solusis` ENABLE KEYS */;

-- Dumping data for table pneumoniasugeno.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'rija', 'rija@gmail.com', 'dokter', '$2y$10$P7wzTAqquHwOZdFKUMKoreRiP9Lxtj7SxscXyF5Q5x4gpGYNSV.pe', '4HMo4EzfXYBhCHjKapVWDn9IoTVurJTuiRTCnEMwRYD7wza0q9Kii6BExJGP', '2018-05-10 10:12:03', '2018-05-10 10:12:03');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
