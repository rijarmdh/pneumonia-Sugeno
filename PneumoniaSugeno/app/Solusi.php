<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solusi extends Model
{
    protected $primaryKey = 'id_solusi';

    protected $fillable = ['nama'];

    public function gejalas(){
        return $this->hasOne('App\Gejala', 'id_solusi', 'id_solusi');
    }
}
