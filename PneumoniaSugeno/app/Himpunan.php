<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Himpunan extends Model
{
    protected $primaryKey = 'id_himpunan';

    protected $fillable = ['id_himpunan', 'nama_himpunan', 'batasbawah','batastengah_a', 'batastengah_b', 'batasatas'];

    public function daftar_variables(){
        return $this->belongsTo('App\Daftar_Variabel', 'id_variable', 'id_variable');
    }
}
