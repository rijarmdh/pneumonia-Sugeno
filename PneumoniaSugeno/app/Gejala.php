<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gejala extends Model
{
    protected $primaryKey = 'id_gejala';

    protected $fillable = ['id_pasien', 'suhu', 'nadi', 'pernafasan', 'usia', 'pao2', 'sistolik', 'ph', 'bun', 'natrium', 'glukosa', 'hematokrit', 'efusi_pleura', 'keganasan', 'penyakit_hati', 'jantung', 'serebrovaskular', 'ginjal', 'gangguan_kesadaran', 'nilai_z', 'pneumonia'];

    protected $hidden = [
      'remember_token',
    ];

    public function pasien(){
      return $this->belongsTo('App\Pasien', 'id_pasien', 'id_pasien');
    }

    public function solusi(){
        return $this->belongsTo('App\Solusi', 'id_solusi', 'id_solusi');
    }
}
