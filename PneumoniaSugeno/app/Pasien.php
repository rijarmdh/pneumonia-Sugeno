<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $primaryKey = 'id_pasien';

    protected $fillable = ['nama', 'nama_panggilan', 'alamat', 'no_telepon', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'status_perkawinan', 'kewarganegaraan', 'agama', 'pekerjaan', 'email','pendidikan','nama_kerabat', 'hubungan', 'nomor_telepon'];

    protected $hidden = [
      'remember_token',
    ];

    public function gejala(){
        return $this->hasMany('App\Gejala', 'id_pasien', 'id_pasien');
    }
}
