<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aturan extends Model
{
    protected $primaryKey = 'id_aturan';

    protected $fillable = ['nama_aturan', 'suhu', 'nadi','pernafasan', 'usia', 'pao2', 'sistolik', 'ph', 'bun', 'natrium', 'glukosa', 'hematokrit', 'efusi_pleura', 'keganasan', 'penyakit_hati', 'jantung', 'serebrovaskular', 'ginjal', 'gangguan_kesadaran', 'pneumonia'];

    protected $hidden = [
      'remember_token',
    ];
}
