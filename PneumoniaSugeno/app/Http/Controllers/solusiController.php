<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solusi;
use DB;
class solusiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Solusi::all();

        return view('solusi.index', compact('data'));
        // return $data; 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_solusi
     * @return \Illuminate\Http\Response
     */
    public function show($id_solusi)
    {
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_solusi
     * @return \Illuminate\Http\Response
     */
    public function edit($id_solusi)
    {
        $solusi = Solusi::findOrFail($id_solusi);

        return view('solusi.edit', compact('solusi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id_solusi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_solusi)
    {
        $solusi = Solusi::findOrFail($id_solusi);
        $solusi->update($request->all());

        return redirect()->route('solusi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id_solusi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_solusi)
    {
        //
    }
}
