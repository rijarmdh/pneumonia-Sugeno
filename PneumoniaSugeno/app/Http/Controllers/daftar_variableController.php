<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Daftar_Variabel;
use App\Himpunan;

class daftar_variableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Daftar_Variabel::all();
        return view('datahimpunan.indexVar', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_variable
     * @return \Illuminate\Http\Response
     */
    public function show($id_variable)
    {
        $data = Himpunan::all()
                ->where('id_variable', $id_variable);
        // return $data;

        return view('datahimpunan.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_variable
     * @return \Illuminate\Http\Response
     */
    public function edit($id_variable)
    {
        $data = Himpunan::findOrFail($id_variable);

        return view('datahimpunan.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id_variable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_variable)
    {
        $data = Himpunan::findOrFail($id_variable);
        $data->update($request->all());

        return redirect()->route('daftarvariabel.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id_variable
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_variable)
    {
        //
    }
}
