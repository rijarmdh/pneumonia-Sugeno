<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aturan;

class aturanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aturan = Aturan::all();
        return view('aturan.index', compact('aturan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aturan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'nama_aturan' => 'required|unique:aturans',
            'pneumonia' => 'required|numeric'
        ]);
        
        Aturan::create($request->all());

        return redirect()->route('aturan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_aturan
     * @return \Illuminate\Http\Response
     */
    public function show($id_aturan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_aturan
     * @return \Illuminate\Http\Response
     */
    public function edit($id_aturan)
    {
        $aturan = Aturan::findOrFail($id_aturan);

        return view('aturan.edit', compact('aturan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id_aturan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_aturan)
    {
        $aturan  = Aturan::findOrFail($id_aturan);

        $aturan->update($request->all());

        return redirect()->route('aturan.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id_aturan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_aturan)
    {
        Aturan::destroy($id_aturan);

        return redirect()->route('aturan.index');
    }
}
