<?php

namespace App\Http\Controllers;


// require '../vendor/autoload.php';

use Illuminate\Http\Request;
use App\Gejala;
use App\Himpunan;
use App\Aturan;
use App\Pasien;
use Illuminate\Support\Facades\DB;
use App\Solusi;
use Carbon;

class gejalaController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct(){
		$this->middleware('jabatan')->only('create', 'hasilkonsultasi', 'hitung');
	}

	public function index(Request $request){   
	  
		// $rekammedis = Gejala::all();
		// $rekammedis = DB::table('gejalas')
		// 				->join('pasiens', 'gejalas.id_pasien', '=', 'pasiens.id_pasien')
		// 				->select('gejalas.*', 'pasiens.nama')
		// 				->get();

		$search	=	\Request::get('search');
		$rekammedis = Pasien::where('id_pasien', 'like', '%'.$search.'%')
						->orWhere('nama', 'like', '%'.$search.'%')
						->orWhere('alamat', 'like', '%'.$search.'%')
						->orWhere('tempat_lahir', 'like', '%'.$search.'%')
						->orWhere('tanggal_lahir', 'like', '%'.$search.'%')
						->orWhere('jenis_kelamin', 'like', '%'.$search.'%')
						->paginate(10);
		
		return $rekammedis;
	}

	public function riwayat($id_pasien){
		$riwayat = Gejala::where('id_pasien', $id_pasien)->get();

		return $riwayat;
	}

	public function hasil(){
		$hasil = DB::table('gejalas')
						->join('pasiens', 'gejalas.id_pasien', '=', 'pasiens.id_pasien')					
						->select('gejalas.*', 'pasiens.nama')
		                ->latest()->first();

		// return view('Hasil Konsultasi.hasil', compact('hasil'));
        return $hasil;
    }

	public function hasilkonsultasi(){
		$lastgejala = DB::table('gejalas')
						->join('pasiens', 'gejalas.id_pasien', '=', 'pasiens.id_pasien')					
						->select('gejalas.*', 'pasiens.nama')
		->latest()->first();

		// return view('Hasil Konsultasi.index', compact('lastgejala'));
        return $lastgejala;
    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function create()
	{
		return view('gejala.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
   
	public function hitung(Request $request){
	
		$tanggallahir = Pasien::where('id_pasien', $request->input('id_pasien'))->pluck('tanggal_lahir')->first();
		
		$now = date('d-m-Y');
		$diff = date_diff(date_create($tanggallahir), date_create($now))->format('%y');
		$diff = intval($diff);

		/// var_dump($now, $tanggallahir, $diff);die;

		//suhu
		$suhudingin =   $this->suhudingin($request->input('suhu'));
		$suhunormal =   $this->suhunormal($request->input('suhu'));
		$suhutinggi =   $this->suhutinggi($request->input('suhu'));
		
		$suhu['rendah'] = $suhudingin;
		$suhu['normal'] = $suhunormal;
		$suhu['panas'] = $suhutinggi;

		//nadi
		$nadirendah = $this->nadirendah($request->input('nadi'));
		$nadinormal = $this->nadinormal($request->input('nadi'));
		$naditinggi = $this->naditinggi($request->input('nadi'));  

		$nadi['rendah'] = 	$nadirendah;
		$nadi['normal'] =	$nadinormal;
		$nadi['tinggi'] =	$naditinggi;

		//pernafasan
		$pernafasanlemah = $this->pernafasanlemah($request->input('pernafasan'));
		$pernafasannormal = $this->pernafasannormal($request->input('pernafasan'));
		$pernafasancepat = $this->pernafasancepat($request->input('pernafasan'));

		$pernafasan['lemah'] = $pernafasanlemah;
		$pernafasan['normal'] = $pernafasannormal;
		$pernafasan['cepat'] = $pernafasancepat;
		
		//usia
		$usiamuda   = $this->usiamuda($diff);
		$usiadewasa = $this->usiadewasa($diff);
		$usialansia = $this->usialansia($diff);	

		$usia['muda'] = $usiamuda;
		$usia['dewasa'] = $usiadewasa;
		$usia['lansia'] = $usialansia;
		// var_dump($usia);die;

		//PAO2
		$pao2hipoksia = $this->pao2hipoksia($request->input('pao2'));
		$pao2normal = $this->pao2normal($request->input('pao2'));

		$pao2['hipoksia'] = $pao2hipoksia;
		$pao2['normal'] = $pao2normal;

		//SISTOLIK
		$sistolikrendah = $this->sistolikrendah($request->input('sistolik'));
		$sistoliknormal = $this->sistoliknormal($request->input('sistolik'));
		$sistoliktinggi = $this->sistoliktinggi($request->input('sistolik'));

		$sistolik['rendah'] = $sistolikrendah;
		$sistolik['normal'] = $sistoliknormal;
		$sistolik['tinggi'] = $sistoliktinggi;

		//PH
		$phasam = $this->phasam($request->input('ph'));
		$phnormal = $this->phnormal($request->input('ph'));
		$phbasa = $this->phbasa($request->input(' ph'));

		$ph['asam'] = $phasam;
		$ph['normal'] = $phnormal;
		$ph['basa'] = $phbasa;

		//BLOOD UREA NITROGEN
		$bunnormal = $this->bunnormal($request->input('bun'));
		$buntinggi = $this->buntinggi($request->input('bun'));

		$bun['normal'] = $bunnormal;
		$bun['tinggi'] = $buntinggi;

		//NATRIUM
		$natriumrendah = $this->natriumrendah($request->input('natrium'));
		$natriumnormal = $this->natriumnormal($request->input('natrium'));
		$natriumtinggi = $this->natriumtinggi($request->input('natrium'));

		$natrium['rendah'] = $natriumrendah;
		$natrium['normal'] = $natriumnormal;
		$natrium['tinggi'] = $natriumtinggi;

		//GLUKOSA
		$glukosarendah = $this->glukosarendah($request->input('glukosa'));
		$glukosanormal = $this->glukosanormal($request->input('glukosa'));
		$glukosatinggi = $this->glukosatinggi($request->input('glukosa'));

		$glukosa['rendah'] = $glukosarendah;
		$glukosa['normal'] = $glukosanormal;
		$glukosa['tinggi'] = $glukosatinggi;

		//HEMATOKRIT
		$hematokritrendah = $this->hematokritrendah($request->input('hematokrit'));
		$hematokritnormal = $this->hematokritnormal($request->input('hematokrit'));
		$hematokrittinggi = $this->hematokrittinggi($request->input('hematokrit'));

		$hematokrit['rendah'] = $hematokritrendah;
		$hematokrit['normal'] = $hematokritnormal;
		$hematokrit['tinggi'] = $hematokrittinggi;

		//EFUSI PLEURA
		$efusipleuraTIDAK = $this->efusitidak($request->input('efusi_pleura'));
		$efusipleuraYA = $this->efusiya($request->input('efusi_pleura'));
		
		$efusipleura['Ya'] = $efusipleuraYA;
		$efusipleura['Tidak'] = $efusipleuraTIDAK;

		//KEGANASAN
		$keganasantidak = $this->keganasantidak($request->input('keganasan'));
		$keganasanya = $this->keganasanya($request->input('keganasan'));

		$keganasan['Tidak'] = $keganasantidak;
		$keganasan['Ya'] = $keganasanya;

		//PENYAKIT HATI
		$penyakit_hatitidak = $this->penyakit_hatitidak($request->input('penyakit_hati'));
		$penyakit_hatiya = $this->penyakit_hatiya($request->input('penyakit_hati'));
		
		$penyakit_hati['Tidak'] =  $penyakit_hatitidak;
		$penyakit_hati['Ya'] = $penyakit_hatiya;
		
		//PENYAKIT JANTUNG
		$penyakitjantungtidak = $this->penyakitjantungtidak($request->input('jantung'));
		$penyakitjantungya = $this->penyakitjantungya($request->input('jantung'));

		$penyakitjantung['Ya'] = $penyakitjantungya;
		$penyakitjantung['Tidak'] = $penyakitjantungtidak;
		
		// Riwayat Serebrovaskular 
		$serebrovaskulartidak = $this->serebrovaskulartidak($request->input('serebrovaskular'));
		$serebrovaskularya = $this->serebrovaskularya($request->input('serebrovaskular'));

		$serebrovaskular['Ya'] = $serebrovaskularya;
		$serebrovaskular['Tidak'] = $serebrovaskulartidak;

		//RIWAYAT PENYAKIT GINJAL
		$ginjaltidak = $this->ginjaltidak($request->input('ginjal'));
		$ginjalya = $this->ginjalya($request->input('ginjal'));

		$ginjal['Ya'] = $ginjalya;
		$ginjal['Tidak'] = $ginjaltidak;
		
		//GANGGUAN KESADARAN
		$gangguankesadarantidak = $this->gangguankesadarantidak($request->input('gangguan_kesadaran'));
		$gangguankesadaranya = $this->gangguankesadaranya($request->input('gangguan_kesadaran'));
 	
 		$gangguankesadaran['Ya'] = $gangguankesadaranya;
		$gangguankesadaran['Tidak'] = $gangguankesadarantidak;
	
		//Memanggil Semua Data aturan
		$aturan = Aturan::all();

		$aturanhasil = array();
		// $predikat = array();
		// $nilaiz = array();

		//cari nilai minimum dari proses fuzzyfikasi dan berdasarkan aturan.
		foreach ($aturan as $key => $aturans) {
		
			$predikat[$aturans['nama_aturan']] = min(array(
				$usia[$aturans['usia']], 
				$keganasan[$aturans['keganasan']],
				$penyakitjantung[$aturans['jantung']], 
				$serebrovaskular[$aturans['serebrovaskular']], 
				$ginjal[$aturans['ginjal']], 
				$gangguankesadaran[$aturans['gangguan_kesadaran']],
				$penyakit_hati[$aturans['penyakit_hati']],
				$pernafasan[$aturans['pernafasan']],
				$sistolik[$aturans['sistolik']], 
				$suhu[$aturans['suhu']],
				$nadi[$aturans['nadi']],
				$ph[$aturans['ph']],
				$bun[$aturans['bun']],
				$natrium[$aturans['natrium']], 
				$glukosa[$aturans['glukosa']], 
				$hematokrit[$aturans['hematokrit']], 
				$pao2[$aturans['pao2']], 
				$efusipleura[$aturans['efusi_pleura']],				
				)
			);

			$pneumoniaberat = $this->pneumoniaberat($aturans['pneumonia'],$predikat[$aturans['nama_aturan']]);
			$pneumoniaringan = $this->pneumoniaringan($aturans['pneumonia'],$predikat[$aturans['nama_aturan']]);


			// // nilai z tiap aturan
			// if($aturans->pneumonia == 'berat'){
			// 	$nilaiz[$aturans['nama_aturan']] = $pneumoniaberat ;	
			// }
			// else{
			// 	$nilaiz[$aturans['nama_aturan']] = $pneumoniaringan;		
			// }

			// dd($predikat, $nilaiz);die;
		}

		$hasilakhir = $this->defuzifikasi($predikat, $nilaiz);	

		//mengambil data himpunan pneumonia
		// $batasbawahringan = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Ringan')->pluck('batasbawah')->first();
		// $batasatasringan = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Ringan')->pluck('batasatas')->first();
		// $batasbawahberat = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Berat')->pluck('batasbawah')->first();
		// $batasatasberat = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Berat')->pluck('batasatas')->first();
		
		//kesimpulan penyakit
		// if($hasilakhir >= $batasbawahringan && $hasilakhir <= 105){
		// 	$kesimpulanpenyakit = 'Pneumonia ringan';
		// }
		// elseif($hasilakhir >= 105){
		// 	$kesimpulanpenyakit = 'Pneumonia berat';
		// }else{
		// 	$kesimpulanpenyakit = 'tidak terprediksi';
		// }
	 
		// //penanganan dan saran perawatan
		//  if($kesimpulanpenyakit == 'Pneumonia berat'){
		//  	$solusi = Solusi::where('id', 1)->pluck('nama')->first();
		//  }
		//  else{
		//  	$solusi = Solusi::where('id', 3)->pluck('nama')->first();
		//  }

		 // debuging 

		// foreach ($predikat as $key => $value) {
		// $keys = 'R'.($key+1); //gadipake
		// $atas[] = $predikat[$key] * $nilaiz[$key]; //???????????????????????????
		// }

		// $defuzifikasi = array_sum($atas);
		// print_r($usia);
		// print_r($predikat);
		// print_r($nilaiz);
		// print_r($atas);
		// print_r(array_sum($predikat)); echo "<br>";
		// print_r($defuzifikasi);echo "<br>";
		// print_r($hasilakhir);echo "<br>";
		// print_r($kesimpulanpenyakit); echo "<br>";
		// print_r($solusi);
		// die();

		$gejala = new Gejala;
		$gejala->id_pasien = $request->input('id_pasien');
		$gejala->suhu = $request->input('suhu');
		$gejala->nadi = $request->input('nadi');
		$gejala->pernafasan = $request->input('pernafasan');
		$gejala->usia = $diff;
		$gejala->pao2 = $request->input('pao2');
		$gejala->sistolik = $request->input('sistolik');
		$gejala->ph = $request->input('ph');
		$gejala->bun = $request->input('bun');
		$gejala->natrium = $request->input('natrium');
		$gejala->glukosa = $request->input('glukosa');
		$gejala->hematokrit = $request->input('hematokrit');
		$gejala->efusi_pleura = $request->input('efusi_pleura');
		$gejala->keganasan = $request->input('keganasan');
		$gejala->penyakit_hati = $request->input('penyakit_hati');
		$gejala->jantung = $request->input('jantung');
		$gejala->serebrovaskular = $request->input('serebrovaskular');
		$gejala->ginjal = $request->input('ginjal');
		$gejala->gangguan_kesadaran = $request->input('gangguan_kesadaran');
		$gejala->nilaiz = $hasilakhir;
		$gejala->pneumonia = $kesimpulanpenyakit;
		$gejala->solusi = $solusi;
		$gejala->save();

		

        // return redirect()->action('gejalaController@hasil');
        return $gejala;
	}

	//kompisisi aturan
	// public function pneumonia($predikat, $aturan){
	// 	$batasbawahringan = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Ringan')->pluck('batasbawah')->first();
	// 	$batasatasringan = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Ringan')->pluck('batasatas')->first();
	// 	$batasbawahberat = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Berat')->pluck('batasbawah')->first();
	// 	$batasatasberat = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Berat')->pluck('batasatas')->first();

	// 	// $aturans = Aturan::pluck('pneumonia')->first();
	// 	if($aturan->where('pneumonia', 'ringan')){
	// 		$nilaiz = round($predikat * ($batasatasberat + $batasbawahberat) + $batasbawahberat ,2);			
	// 	}else{
	// 		$nilaiz = round($batasatasringan - $predikat * ($batasatasringan - $batasbawahringan)  ,2)  ;			
	// 	}

	// 	return $nilaiz;
	// }

	// public function pneumoniaringan($aturan, $predikat){		
	// 	$batasbawahringan = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Ringan')->pluck('batasbawah')->first();
	// 	$batasatasringan = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Ringan')->pluck('batasatas')->first();

	// 	$nilaiz = round($batasatasringan - $predikat * ($batasatasringan - $batasbawahringan)  ,2);

	// 	return $nilaiz;
	// }

	// public function pneumoniaberat($aturan, $predikat){
	// 	//BATAS PNEUMONIA  BERAT
	// 	$batasbawahberat = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Berat')->pluck('batasbawah')->first();
	// 	$batasatasberat = Himpunan::where('nama_himpunan', 'Tingkat Pneumonia Berat')->pluck('batasatas')->first();

	// 	$nilaiz = round($batasbawahberat + $predikat * ($batasatasberat - $batasbawahberat)  ,2); // direvisi
	
	// 	return $nilaiz;
	// }
	
	//HASIL AKHIR
	public function defuzifikasi($predikat, $nilaiz){
		foreach ($predikat as $key => $value) {
			$keys = 'R'.($key+1); //Gadipakai; :))))
			$atas[] = $predikat[$key] * $nilaiz[$key]; //?????????????????????????/
		}
		$pembagi = array_sum($atas);
			
		if($pembagi != 0){
			$defuzifikasi  = round($pembagi/array_sum($predikat), 2);
			echo "<script>alert('data berhasil ditambahkan')</script>";

		}else{

			echo "<script>alert('Gagal Melakukan Pemeriksaan karena tidak terdapat di aturan, silahkan tambahkan aturan yang sesuai terlebih dahulu');history.go(-1);</script>";	
		}
		return $defuzifikasi;
	}

	public function suhudingin($angka){
		$batasbawahsuhudingin = Himpunan::where('nama_himpunan', 'Suhu Rendah')->pluck('batasbawah')->first();
		$batasatassuhudingin = Himpunan::where('nama_himpunan', 'Suhu Rendah')->pluck('batasatas')->first();

		if($angka <= $batasbawahsuhudingin)//35\
		{
			$suhudingin = 1;

		}elseif($angka >= $batasbawahsuhudingin && $angka <= $batasatassuhudingin)
		{
			$suhudingin = round(($batasatassuhudingin - $angka)/($batasatassuhudingin - $batasbawahsuhudingin), 2);
		}
		else{
			$suhudingin = 0;
		}

		return $suhudingin;
	}

	//SUHU
	public function suhunormal($angka){

		$batasbawahsuhunormal = Himpunan::where('nama_himpunan', 'Suhu Normal')->pluck('batasbawah')->first();
		$batastengah1suhunormal = Himpunan::where('nama_himpunan', 'Suhu Normal')->pluck('batastengah_a')->first();
		$batastengah2suhunormal = Himpunan::where('nama_himpunan', 'Suhu Normal')->pluck('batastengah_b')->first();
		$batasatassuhunormal = Himpunan::where('nama_himpunan', 'Suhu Normal')->pluck('batasatas')->first();

		if($angka <= $batasbawahsuhunormal)
		{
			$suhunormal = 0;

		}elseif($angka >= $batasbawahsuhunormal && $angka <= $batastengah1suhunormal){

			$suhunormal = round(($angka - $batasbawahsuhunormal)/($batastengah1suhunormal - $batasbawahsuhunormal), 2);

		}elseif( $angka >= $batastengah1suhunormal && $angka <= $batastengah2suhunormal){
			
			$suhunormal = 1;

		}elseif( $angka >= $batastengah2suhunormal && $angka <= $batasatassuhunormal){

			$suhunormal = round(($batasatassuhunormal - $angka)/($batasatassuhunormal -  $batastengah2suhunormal), 2);
		}else{

			$suhunormal = 0;

			}


		return $suhunormal;
	}

	public function suhutinggi($angka){

		$batasbawahsuhutinggi = Himpunan::where('nama_himpunan', 'Suhu Panas')->pluck('batasbawah')->first();
		$batasatassuhutinggi = Himpunan::where('nama_himpunan', 'Suhu Panas')->pluck('batasatas')->first();

		if($angka <= $batasbawahsuhutinggi){

			$suhutinggi =   0;

		}elseif($angka >= $batasbawahsuhutinggi && $angka <= $batasatassuhutinggi){

			$suhutinggi =  round(($angka - $batasbawahsuhutinggi)/($batasatassuhutinggi - $batasbawahsuhutinggi), 2);

		}else{ 

			$suhutinggi =   1;
		}

		return  $suhutinggi;

	}

	//NADI
	public function nadirendah($angka){

		$batasbawahnadirendah = Himpunan::where('nama_himpunan', 'Nadi Rendah')->pluck('batasbawah')->first();
		$batasatasnadirendah = Himpunan::where('nama_himpunan', 'Nadi Rendah')->pluck('batasatas')->first();

		if($angka <= $batasbawahnadirendah){

			$nadirendah = 1;

		}elseif ($angka >= $batasbawahnadirendah && $angka <= $batasatasnadirendah) {
			
			$nadirendah = round(($batasatasnadirendah - $angka)/($batasatasnadirendah - $batasbawahnadirendah), 2);

		}else{
			$nadirendah = 0;
		}

		return $nadirendah;
	}

	public function nadinormal($angka){

		$batasbawahnadinormal = Himpunan::where('nama_himpunan', 'Nadi Normal')->pluck('batasbawah')->first();
		$batastengahanadinormal = Himpunan::where('nama_himpunan', 'Nadi Normal')->pluck('batastengah_a')->first();
		$batastengahbnadinormal = Himpunan::where('nama_himpunan', 'Nadi Normal')->pluck('batastengah_b')->first();
		$batasatasnadinormal = Himpunan::where('nama_himpunan', 'Nadi Normal')->pluck('batasatas')->first();

		if($angka <= $batasbawahnadinormal)
		{
			$nadinormal = 0; 

		}elseif ($angka >= $batasbawahnadinormal  && $angka <= $batastengahanadinormal) {
			$nadinormal = round(($angka - $batasbawahnadinormal)/($batastengahanadinormal - $batasbawahnadinormal), 2);
		}elseif ($angka >= $batastengahanadinormal && $angka <= $batastengahbnadinormal) {
			$nadinormal = 1;
		}elseif ($angka >= $batastengahbnadinormal && $angka <= $batasatasnadinormal) {
			$nadinormal = round(($batasatasnadinormal - $angka)/($batasatasnadinormal - $batastengahbnadinormal), 2);
		}else{
			$nadinormal = 0;
		}

		return $nadinormal;
	}

	public function naditinggi($angka){
		
		$batasbawah = Himpunan::where('nama_himpunan', 'Nadi Tinggi')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Nadi Tinggi')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$naditinggi = 0;
		}elseif ($angka >= $batasbawah && $angka <= $batasatas) 
		{
			$naditinggi = round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}else{
			$naditinggi = 1;
		}

		return $naditinggi;
	}

	//PERNAFASAN
	public function pernafasanlemah($angka){
		
		$batasbawah = Himpunan::where('nama_himpunan', 'Pernafasan Lemah')->pluck('batasbawah')->first();	
		$batasatas = Himpunan::where('nama_himpunan', 'Pernafasan Lemah')->pluck('batasatas')->first();			

		if($angka <= $batasbawah){
			$pernafasanlemah = 1;
		}elseif ($angka >= $batasbawah && $angka <= $batasatas) {
			$pernafasanlemah = round(($batasatas - $angka)/($batasatas- $batasbawah), 2);
		}else{
			$pernafasanlemah = 0;
		}

		return $pernafasanlemah;
	}

	public function pernafasannormal($angka){
		
		$batasbawah = Himpunan::where('nama_himpunan', 'Pernafasan Normal')->pluck('batasbawah')->first();
		$batastengah_a = Himpunan::where('nama_himpunan', 'Pernafasan Normal')->pluck('batastengah_a')->first();
		$batastengah_b = Himpunan::where('nama_himpunan', 'Pernafasan Normal')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Pernafasan Normal')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$pernafasannormal= 0;
		}elseif ($angka >= $batasbawah && $angka <= $batastengah_a) {
			$pernafasannormal = round(($angka - $batasbawah)/($batastengah_a - $batasbawah), 2);
		}elseif ($angka >= $batastengah_a && $angka <= $batastengah_b) {
			$pernafasannormal = 1;
		}elseif($angka >= $batastengah_b && $angka <= $batasatas){
			$pernafasannormal = round(($batasatas - $angka)/($batasatas - $batastengah_b), 2);
		}else{
			$pernafasannormal = 0;
		}

		return $pernafasannormal;
	}

	public function pernafasancepat($angka){
	

		$batasbawah = Himpunan::where('nama_himpunan', 'Pernafasan Cepat')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Pernafasan Cepat')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$pernafasancepat = 0;
		}elseif($angka >= $batasbawah && $angka <= $batasatas){
			$pernafasancepat = round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}else{
			$pernafasancepat = 1;
		}

		return $pernafasancepat;
	}


	//USIA
	public function usiamuda($angka){
		// $batasbawah     = Himpunan::select('batasbawah')->where('nama_himpunan','UsiaMuda');
		// $batasatas      = Himpunan::select('batasatas')->where('nama_himpunan', 'UsiaMuda');

		$batasbawah = Himpunan::where('nama_himpunan', 'Usia Muda')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Usia Muda')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$usiamuda = 1;
		}
		elseif ($angka >= $batasbawah && $angka <= $batasatas) {
			$usiamuda = round(($batasatas - $angka)/($batasatas - $batasbawah), 2);
		}
		else{
			$usiamuda = 0;
		}

		return $usiamuda;
	}

	public function usiadewasa($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'UsiaDewasa');
		// $batastengah = Himpunan::select('batastengah_a')->where('nama_himpunan', 'UsiaDewasa');
		// $batasatas= Himpunan::select('batasatas')->where('nama_himpunan', 'UsiaDewasa');

		$batasbawah = Himpunan::where('nama_himpunan', 'Usia Dewasa')->pluck('batasbawah')->first();
        $batastengaha = Himpunan::where('nama_himpunan', 'Usia Dewasa')->pluck('batastengah_a')->first();
        $batastengahb = Himpunan::where('nama_himpunan', 'Usia Dewasa')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Usia Dewasa')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$usiadewasa = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batastengaha){
			$usiadewasa = round(($angka - $batasbawah)/($batastengaha - $batasbawah), 2);
        }
        elseif($angka >= $batastengaha && $angka <= $batastengahb){
			$usiadewasa = 1;
		}
		elseif($angka >= $batastengahb && $angka <= $batasatas){
			$usiadewasa = round(($batasatas - $angka)/($batasatas - $batastengahb), 2); 
		}
		else{
			$usiadewasa = 0;
		}

		return $usiadewasa;
	}

	public function usialansia($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'UsiaLansia');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'UsiaLansia');

		$batasbawah = Himpunan::where('nama_himpunan', 'Usia Lansia')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Usia Lansia')->pluck('batasatas')->first();
		

		if($angka <= $batasbawah){
			$usialansia = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$usialansia = round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}
		else{
			$usialansia = 1;
		}


		return $usialansia;
	}


	//PAO2
	public function pao2hipoksia($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan','pao2Hipoksia');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'pao2hipoksia');

		$batasbawah = Himpunan::where('nama_himpunan', 'Pao2 Hipoksia')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Pao2 Hipoksia')->pluck('batasatas')->first();
		
		if($angka <= $batasbawah){
			$pao2hipoksia = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$pao2hipoksia = round(($batasatas - $angka)/($batasatas- $batasbawah), 2);
		}
		else{
			$pao2hipoksia = 0;
		}

		return $pao2hipoksia;
	}

	public function pao2normal($angka){
		
		$batasbawah = Himpunan::where('nama_himpunan', 'Pao2 Normal')->pluck('batasbawah')->first();
		$batastengah_a = Himpunan::where('nama_himpunan', 'Pao2 Normal')->pluck('batastengah_a')->first();
		$batastengah_b = Himpunan::where('nama_himpunan', 'Pao2 Normal')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Pao2 Normal')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$pao2normal = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batastengah_a){
			$pao2normal = round(($angka - $batasbawah)/($batastengah_a - $batasbawah), 2);
		}
		elseif($angka >= $batastengah_a && $angka <= $batastengah_b){
			$pao2normal = 1;
		}
		elseif($angka >= $batastengah_b && $angka <= $batasatas){
			$pao2normal = round(($batasatas - $angka)/($batasatas - $batastengah_b), 2);
		}
		else{
			$pao2normal = 0;
		}

		return $pao2normal;
	}


	//SISTOLIK
	public function sistolikrendah($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'sistolikRendah');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'sistolikRendah');

		$batasbawah = Himpunan::where('nama_himpunan', 'Sistolik Rendah')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Sistolik Rendah')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$sisrendah = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$sisrendah = round(($batasatas- $angka)/($batasatas - $batasbawah), 2);
		}
		else{
			$sisrendah = 0;
		}

		return $sisrendah;
	}

	public function sistoliknormal($angka){

		$batasbawah = Himpunan::where('nama_himpunan', 'Sistolik Normal')->pluck('batasbawah')->first();
		$batastengah_a = Himpunan::where('nama_himpunan', 'Sistolik Normal')->pluck('batastengah_a')->first();
		$batastengah_b = Himpunan::where('nama_himpunan', 'Sistolik Normal')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Sistolik Normal')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$sisnormal = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batastengah_a){
			$sisnormal = round(($angka -$batasbawah)/($batastengah_a - $batasbawah), 2);
		}
		elseif($angka >= $batastengah_a && $angka <= $batastengah_b){
			$sisnormal = 1;
		}
		elseif($angka >= $batastengah_b && $angka <= $batasatas){
			$sisnormal = round(($batasatas - $angka)/($batasatas - $batastengah_b), 2);
		}
		else{
			$sisnormal = 0;
		}

		return $sisnormal;
	}

	public function sistoliktinggi($angka){
	
		$batasbawah = Himpunan::where('nama_himpunan', 'Sistolik Tinggi')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Sistolik Tinggi')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$sistinggi = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$sistinggi = round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}
		else{
			$sistinggi = 1;
		}

		return $sistinggi;
	}


	//PH ASAM
	// B E L U M    	D I 		T  E   S  ~~~~~~~~~~~~~~~~~~~~~~~~
	public function phasam($angka){ 
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'phAsam');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'phAsam');

		$batasbawah = Himpunan::where('nama_himpunan', 'Ph Asam')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Ph Asam')->pluck('batasatas')->first();

		if( $angka <= $batasbawah){
			$phasam = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$phasam = round(($batasatas - $angka)/($batasatas - $batasbawah), 2);
		}
		else{
			$phasam = 0;
		}

		return $phasam;
	}

	public function phnormal($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'phNormal');
		// $batastengah_a = Himpunan::select('batastengah_a')->where('nama_himpunan', 'phNormal');
		// $batastengah_b = Himpunan::select('batastengah_b')->where('nama_himpunan', 'phNormal');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'phNormal');

		$batasbawah = Himpunan::where('nama_himpunan', 'Ph Normal')->pluck('batasbawah')->first();
		$batastengah_a = Himpunan::where('nama_himpunan', 'Ph Normal')->pluck('batastengah_a')->first();
		$batastengah_b = Himpunan::where('nama_himpunan', 'Ph Normal')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Ph Normal')->pluck('batasatas')->first();
		


		if($angka <= $batasbawah){
			$phnormal = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batastengah_a){
			$phnormal = round(($angka - $batasbawah)/($batastengah_a - $batasbawah), 2);
		}
		elseif($angka >= $batastengah_a && $angka <= $batastengah_b){
			$phnormal = 1;
		}
		elseif($angka >= $batastengah_b && $angka <=$batasatas ){

			$phnormal = round(($batasatas - $angka)/($batasatas - $batastengah_b), 2);
		}
		else{
			$phnormal = 0;
		}

		return $phnormal;
	}

	public function phbasa($angka){

		$batasbawah = Himpunan::where('nama_himpunan', 'Ph Basa')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Ph Basa')->pluck('batasatas')->first();
			
		if($angka <= $batasbawah){
			$phbasa = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$phbasa = round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}
		else{
			$phbasa = 1;
		}

		return $phbasa;
	}


	//BLOOD UREA NITROGEN
	// public function bunrendah($angka){
	// 	// $batasbawah =   Himpunan::select('batasbawah')->where('nama_himpunan', 'BUNrendah');
	// 	// $batasatas  =   Himpunan::select('batasatas')->where('nama_himpunan', 'BUNrendah');

	// 	$batasbawah = Himpunan::where('nama_himpunan', 'BUNrendah')->pluck('batasbawah')->first();
	// 	$batasatas = Himpunan::where('nama_himpunan', 'BUNrendah')->pluck('batasatas')->first();
	
	// 	if($angka <= $batasbawah){
	// 		$bunrendah = 1;
	// 	}
	// 	elseif($angka >= $batasbawah && $angka <= $batasatas){
	// 		$bunrendah = round(($batasatas - $angka)/($batasatas- $batasbawah), 2);
	// 	}
	// 	else{
	// 		$bunrendah = 0;
	// 	}

	// 	return $bunrendah;
	// }

	public function bunnormal($angka){

		$batasbawah = Himpunan::where('nama_himpunan', 'bunNormal')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'bunNormal')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$bunnormal = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$bunnormal = round(($batasatas - $angka)/($batasatas - $batasbawah), 2);
		}
		else{
			$bunnormal = 0;
		}

		return $bunnormal;
	}

	public function buntinggi($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'bunTinggi');
		// $batasatas  = Himpunan::select('batasatas')->where('nama_himpunan', 'bunTinggi');
		$batasbawah = Himpunan::where('nama_himpunan', 'bunTinggi')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'bunTinggi')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$buntinggi = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas ){
			$buntinggi = round(($angka - $batasbawah)/($batasatas -$batasbawah), 2);
		}
		else{
			$buntinggi = 1;
		}

		return $buntinggi;
	}


	//NATRIUM
	public function natriumrendah($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'natriumRendah');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'natriumRendah');

		$batasbawah = Himpunan::where('nama_himpunan', 'natriumRendah')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'natriumRendah')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$natriumrendah = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$natriumrendah = round(($batasatas - $angka)/($batasatas - $batasbawah), 2);
		}
		else{
			$natriumrendah = 0;
		}

		return $natriumrendah;
	}

	public function natriumnormal($angka){

		$batasbawah = Himpunan::where('nama_himpunan', 'natriumNormal')->pluck('batasbawah')->first();
		$batastengah_a = Himpunan::where('nama_himpunan', 'natriumNormal')->pluck('batastengah_a')->first();
		$batastengah_b = Himpunan::where('nama_himpunan', 'natriumNormal')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'natriumNormal')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$natnormal = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batastengah_a){
			$natnormal = round(($angka - $batasbawah)/($batastengah_a - $batasbawah), 2);
		}
		elseif($angka >= $batastengah_a && $angka <= $batastengah_b){
			$natnormal = 1;
		}
		elseif($angka >= $batastengah_b && $angka <= $batasatas){
			$natnormal = round(($batasatas - $angka)/($batasatas - $batastengah_b), 2);
		}
		else{
			$natnormal = 0;
		}

		return $natnormal;
	}

	public function natriumtinggi($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'natriumTinggi');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'natriumTinggi');

		$batasbawah = Himpunan::where('nama_himpunan', 'natriumTinggi')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'natriumTinggi')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$nattinggi = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$nattinggi= round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}
		else{
			$nattinggi = 1;
		}

		return $nattinggi;
	}


	//GLUKOSA
	public function glukosarendah($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'glukosaRendah');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'glukosaRendah');

		$batasbawah = Himpunan::where('nama_himpunan', 'glukosaRendah')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'glukosaRendah')->pluck('batasatas')->first();

		if($angka <= $batasbawah){

			$glukrendah = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$glukrendah = round(($batasatas - $angka)/($batasatas - $batasbawah), 2);
		}
		else{
			$glukrendah = 0;
		}

		return $glukrendah;
	}

	public function glukosanormal($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'glukosaNormal');
		// $batastengah_a = Himpunan::select('batastengah_a')->where('nama_himpunan', 'glukosaNormal');
		// $batastengah_b = Himpunan::Select('batastengah_b')->where('nama_himpunan', 'glukosaNormal');

		$batasbawah = Himpunan::where('nama_himpunan', 'glukosaNormal')->pluck('batasbawah')->first();
		$batastengah_a = Himpunan::where('nama_himpunan', 'glukosaNormal')->pluck('batastengah_a')->first();
		$batastengah_b = Himpunan::where('nama_himpunan', 'glukosaNormal')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'glukosaNormal')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$gluknormal = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batastengah_a){
			$gluknormal = round(($angka - $batasbawah)/($batastengah_a - $batasbawah), 2);
		}
		elseif($angka >= $batastengah_a && $angka <= $batastengah_b){
			$gluknormal = 1;
		}
		elseif($angka >= $batastengah_b && $angka <= $batasatas){
			$gluknormal = round(($batasatas - $angka)/($batasatas - $batastengah_b), 2);
		}
		else{
			$gluknormal = 0;
		}

		return $gluknormal;
	}

	public function glukosatinggi($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'glukosaTinggi');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'glukosaTinggi');

		$batasbawah = Himpunan::where('nama_himpunan', 'glukosaTinggi')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'glukosaTinggi')->pluck('batasatas')->first();

		if($angka <= $batasbawah ){
			$gluktinggi = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$gluktinggi = round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}
		else{
			$gluktinggi = 1;
		}

		return $gluktinggi;
	}


	//HEMATOKRIT
	public function hematokritrendah($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'hematokritRendah');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'hematokritRendah');

		$batasbawah = Himpunan::where('nama_himpunan', 'hematokritRendah')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'hematokritRendah')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$hemarendah = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$hemarendah = round(($batasatas - $angka)/($batasatas - $batasbawah), 2);
		}
		else{
			$hemarendah = 0;
		}

		return $hemarendah;
	}

	public function hematokritnormal($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'hematokritNormal');
		// $batastengah_a = Himpunan::select('batastengah_a')->where('nama_himpunan', 'hematokritNormal');
		// $batastengah_b = Himpunan::select('batastengah_b')->where('nama_himpunan', 'hematokritNormal');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'hematokritNormal');

		$batasbawah = Himpunan::where('nama_himpunan', 'hematokritNormal')->pluck('batasbawah')->first();
		$batastengah_a = Himpunan::where('nama_himpunan', 'hematokritNormal')->pluck('batastengah_a')->first();
		$batastengah_b = Himpunan::where('nama_himpunan', 'hematokritNormal')->pluck('batastengah_b')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'hematokritNormal')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$hemanormal = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batastengah_a){
			$hemanormal= round(($angka - $batasbawah)/($batastengah_a - $batasbawah), 2);
		}
		elseif($angka >= $batastengah_a && $angka <= $batastengah_b){
			$hemanormal = 1;
		}
		elseif($angka >= $batastengah_b && $angka <= $batasatas){
			$hemanormal = round(($batasatas - $angka)/($batasatas - $batastengah_b), 2);
		}
		else{
			$hemanormal = 0;
		}

		return $hemanormal;
	}

	public function hematokrittinggi($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'hematokritTinggi');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'hematokritTinggi');

		$batasbawah = Himpunan::where('nama_himpunan', 'hematokritTinggi')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'hematokritTinggi')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$hematinggi = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$hematinggi = round(($angka - $batasbawah)/($batasatas - $batasbawah), 2);
		}
		else{
			$hematinggi = 1;
		}

		return $hematinggi;
	}

	//EFUSI PLEURA
	public function efusitidak($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'efusipleuraTidak');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'efusipleuraTidak');

		$batasbawah = Himpunan::where('nama_himpunan', 'efusipleuraTIDAK')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'efusipleuraTIDAK')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$efusitidak = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$efusitidak = ($batasatas - $angka)/($batasatas- $batasbawah);
		}
		else{
			$efusitidak = 0;
		}

		return $efusitidak;
	}

	public function efusiya($angka){
		
		$batasbawah = Himpunan::where('nama_himpunan', 'efusipleuraYA')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'efusipleuraYA')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$efusiya = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$efusiya = ($angka - $batasbawah)/($batasatas- $batasbawah);
		}
		else{
			$efusiya = 1;
		}

		return $efusiya;
	}


	//KEGANASAN
	public function keganasantidak($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'keganasanTidak');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'keganasanTidak');

		$batasbawah = Himpunan::where('nama_himpunan', 'keganasanTidak')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'keganasanTidak')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$keganasantidak = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$keganasantidak = ($batasatas - $angka)/($batasatas - $batasbawah);
		}
		else{
			$keganasantidak = 0;
		}

		return $keganasantidak;
	}
	
	public function keganasanya($angka){
		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'keganasanYa');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'keganasanYa');

		$batasbawah = Himpunan::where('nama_himpunan', 'keganasanYa')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'keganasanYa')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$keganasanya = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$keganasanya = ($angka - $batasbawah)/($batasatas - $batasbawah);
		}
		else{
			$keganasanya= 1;
		}

		return $keganasanya;
	}

	//RIWAYAT PENYAKIT HATI
	public function penyakit_hatitidak($angka){

		$batasbawah = Himpunan::where('nama_himpunan', 'riwayat penyakit hati Tidak')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'riwayat penyakit hati Tidak')->pluck('batasatas')->first();

		if($angka <= $batasbawah ){

			$penyakit_hatitidak = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){

			$penyakit_hatitidak = ($batasatas - $angka)/($batasatas - $batasbawah);
		}
		else{
			$penyakit_hatitidak = 0;
		}

		return $penyakit_hatitidak;

		dd($penyakit_hatitidak);
	}

	public function penyakit_hatiya($angka){

		$batasbawah = Himpunan::where('nama_himpunan', 'riwayat penyakit hati Ya')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'riwayat penyakit hati Ya')->pluck('batasatas')->first();

		if($angka <= $batasbawah){
			$penyakit_hatiya = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$penyakit_hatiya = ($angka - $batasbawah)/($batasatas  - $batasbawah);
		}
		else{
			$penyakit_hatiya = 1;
		}

		return $penyakit_hatiya;
	}


	//RIWAYAT PEYAKIT JANTUNG TIDAK
	public function penyakitjantungtidak($angka){

		$batasbawah = Himpunan::where('nama_himpunan', 'Riwayat Jantung Kongestif Tidak')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Riwayat Jantung Kongestif Tidak')->pluck('batasatas')->first();


		if($angka <= $batasbawah ){

			$penyakitjantungtidak = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){

			$penyakitjantungtidak = ($batasatas - $angka)/($batasatas - $batasbawah);
		}
		else{

			$penyakitjantungtidak = 0;
		}

		return $penyakitjantungtidak;
	}

	public function penyakitjantungya($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'Riwayat Jantung Kongestif Ya');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'Riwayat Jantung Kongestif Ya');

		$batasbawah = Himpunan::where('nama_himpunan', 'Riwayat Jantung Kongestif Ya')->pluck('batasbawah')->first();
		$batasatas= Himpunan::where('nama_himpunan', 'Riwayat Jantung Kongestif Ya')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$penyakitjantungya = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$penyakitjantungya = ($angka - $batasbawah)/($batasatas  - $batasbawah);
		}
		else{
			$penyakitjantungya = 1;
		}

		return $penyakitjantungya;
	}


	//SEREBROVAKULAR
	public function serebrovaskulartidak($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'Riwayat serebrovaskular Tidak');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'Riwayat serebrovaskular Tidak');

		$batasbawah = Himpunan::where('nama_himpunan', 'Riwayat serebrovaskular Tidak')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Riwayat serebrovaskular Tidak')->pluck('batasatas')->first();

		if($angka <= $batasbawah ){

			$serebrotidak = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){

			$serebrotidak = ($batasatas - $angka)/($batasatas - $batasbawah);
		}
		else{

			$serebrotidak = 0;
		}

		return $serebrotidak;
	}

	public function serebrovaskularya($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'Riwayat serebrovaskular Ya');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'Riwayat serebrovaskular Ya');

		$batasbawah = Himpunan::where('nama_himpunan', 'Riwayat serebrovaskular Ya')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Riwayat serebrovaskular Ya')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$serebroya = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$serebroya = ($angka - $batasbawah)/($batasatas  - $batasbawah);
		}
		else{
			$serebroya = 1;
		}

		return $serebroya;
	}

	//GINJAL
	public function ginjaltidak($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'riwayat penyakit ginjal Tidak');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'riwayat penyakit ginjal Tidak');

		$batasbawah = Himpunan::where('nama_himpunan', 'riwayat penyakit ginjal Tidak')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'riwayat penyakit ginjal Tidak')->pluck('batasatas')->first();

		if($angka <= $batasbawah ){

			$ginjaltidak = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){

			$ginjaltidak = ($batasatas - $angka)/($batasatas - $batasbawah);
		}
		else{

			$ginjaltidak = 0;
		}

		return $ginjaltidak;
	}

	public function ginjalya($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'riwayat penyakit ginjal Ya');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'riwayat penyakit ginjal Ya');

		$batasbawah = Himpunan::where('nama_himpunan', 'riwayat penyakit ginjal Ya')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'riwayat penyakit ginjal Ya')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$ginjalya = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$ginjalya = ($angka - $batasbawah)/($batasatas  - $batasbawah);
		}
		else{
			$ginjalya = 1;
		}

		return $ginjalya;
	}


	//GANGGUAN KESADARAN
	public function gangguankesadarantidak($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'Riwayat Gangguan Kesadaran Tidak');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'Riwayat Gangguan Kesadaran Tidak');

		$batasbawah = Himpunan::where('nama_himpunan', 'Riwayat Gangguan Kesadaran Tidak')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Riwayat Gangguan Kesadaran Tidak')->pluck('batasatas')->first();

		if($angka <= $batasbawah ){

			$sadartidak = 1;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){

			$sadartidak = ($batasatas - $angka)/($batasatas - $batasbawah);
		}
		else{

			$sadartidak = 0;
		}

		return $sadartidak;
	}

	public function gangguankesadaranya($angka){

		// $batasbawah = Himpunan::select('batasbawah')->where('nama_himpunan', 'Riwayat Gangguan Kesadaran Ya');
		// $batasatas = Himpunan::select('batasatas')->where('nama_himpunan', 'Riwayat Gangguan Kesadaran Ya');

		$batasbawah = Himpunan::where('nama_himpunan', 'Riwayat Gangguan Kesadaran Ya')->pluck('batasbawah')->first();
		$batasatas = Himpunan::where('nama_himpunan', 'Riwayat Gangguan Kesadaran Ya')->pluck('batasatas')->first();


		if($angka <= $batasbawah){
			$sadarya = 0;
		}
		elseif($angka >= $batasbawah && $angka <= $batasatas){
			$sadarya = ($angka - $batasbawah)/($batasatas  - $batasbawah);
		}
		else{
			$sadarya = 1;
		}

		return $sadarya;
	}

	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id_gejala)
	{
		$rekmed = Gejala::find($id_gejala);
		// $rekmed = DB::table('gejalas')
		// 			->join('pasiens', 'gejalas.id_pasien', '=', 'pasiens.id_pasien')
		// 			->select('gejalas.*', 'pasiens.nama')
		// 			->find($id_gejala)
		// 			->get();



		return view('Riwayat Rekam Medis.detail', compact('rekmed'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id_gejala)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id_gejala)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id_gejala)
	{
		Gejala::destroy($id_gejala);

		return redirect()->back();
	}
}