<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Daftar_Variabel extends Model
{
    protected $primaryKey = 'id_variable';

    protected $fillable = ['nama'];

    public function himpunans(){
        return $this->hasMany('App\Himpunan', 'id_variable', 'id_variable');
    }
}
