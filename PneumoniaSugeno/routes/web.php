<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// route::get('pasien/data', 'pasienController@getData')->name('pasien.data');
route::resource('pasien', 'pasienController');

route::post('gejala/hitung','gejalaController@hitung');
route::get('gejala/riwayat_rekam_medis','gejalaController@riwayat_rekam_medis');
route::get('gejala/rekam_medis_per_pasien/{id_pasien}','gejalaController@rekam_medis_per_pasien');
route::resource('gejala', 'gejalaController');
route::resource('daftarvariabel', 'daftar_variableController');
route::resource('aturan', 'aturanController');
route::resource('gejala', 'gejalaController');
route::resource('himpunan', 'himpunanController');
route::resource('solusi', 'solusiController');
route::resource('user', 'userController');