@extends('layouts.dashboard')

@section('sidebar')
<!DOCTYPE html>

{{-- <div class="table-responsive" style="margin:0; width: 1050px"> --}}
    <table class="table"  id="users-table">
        <thead>
            <tr>   
                <th>id</th>
                <th>Nama Solusi</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php $i=1; ?>
            @foreach($data as $dt)
                <tr>
                <td><?= $i++ ?></td>
                <td>{{ $dt->nama }}</td>
                {{-- <td>
                    <form method="post" action="{{route('pasien.destroy', $dt->id_pasien)}}" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="DELETE">                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                        <input type="submit" class="btn btn-xs btn-danger" onclick="return confirm('yakin ingin menghapus ?');" value="delete"> 	
                    </form>
                </td> --}}
                <td>
                    <a class="btn btn-xs btn-primary" href="{{route('solusi.edit', $dt->id_solusi)}}">edit</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
{{-- </div> --}}
    
@endsection
}