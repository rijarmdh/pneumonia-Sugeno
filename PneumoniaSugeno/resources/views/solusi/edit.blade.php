@extends('layouts.dashboard')

@section('sidebar')
<div class="container">
        {!! Form::model($solusi, ['url'=>route('solusi.update', $solusi->id_solusi), 'method'=>'put', 'class'=>'form-horizontal']) !!}
         {{ csrf_field() }}
             
             <div class="row">           
                 <div class="col-md-12">
                     <div class="card">
                         <div class="card-header with-border"><h3 class="card-title ">Register Data Pasien</h3></div>
                             <div class="card-body">             
 
                                 <div class="form-group{{ $errors->has('nama_solusi') ? ' has-error' : '' }}">
                                     <div class="col-md-12">
                                         <label for="nama">Nama Solusi</label>
                                     </div>
                                          
                                     <div class=" col-md-12">
                                         <div class="input-group"> 
                                             {{-- <div class="input-group-addon"><i style="color: #b82601" class="fa fa-male" aria-hidden="true"></i></div> --}}
                                             <input id="nama" type="text" class="form-control" name="nama" value="{{$solusi->nama}}" placeholder="Masukkan Nama Pasien" required autofocus>
 
                                              @if ($errors->has('nama'))
                                                  <span class="help-block">
                                                     <strong>{{ $errors->first('nama') }}</strong>
                                                  </span>
                                             @endif
                                         </div>
                                         
                                     </div>
                                 </div>
                                 <br>
                            
                             </div>
                     </div>
                 </div> 
 
                 </div>
 
                 <div class="form-group">
                     <div class="col-md-8 col-md-offset-10">
                         <button type="submit" class="btn btn-primary">
                             Submit
                          </button>
                     </div>                    
                 </div>      
         {!! Form::close() !!}
     </div>
@endsection