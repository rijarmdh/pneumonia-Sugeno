@extends('layouts.dashboard')

@section('sidebar')
<div class="table-responsive" style="margin:0; width: 1050px">
        <table class="table"  id="users-table">
            <thead>
                <tr>   
                    <th>No</th>
                    <th>Nama Variabel</th>
                    <th>Detail Himpunan</th>
                </tr>
            </thead>

            <tbody>
                <?php $i = 1 ?>
                @foreach($data as $datas)
                    <tr>
                        <td><?= $i++?></td>
                        <td>{{$datas->nama}}</td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="{{route('daftarvariabel.show', $datas->id_variable)}}">Himpunan</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</div>
@endsection