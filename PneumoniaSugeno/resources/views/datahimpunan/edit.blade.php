@extends('layouts.dashboard')

@section('sidebar')
<div class="container">
        {!! Form::model($data, ['url'=>route('daftarvariabel.update', $data->id_himpunan), 'method'=>'put', 'class'=>'form-horizontal']) !!}
         {{ csrf_field() }}
             
             <div class="row">           
                 <div class="col-md-12">
                     <div class="card">
                         <div class="card-header with-border"><h3 class="card-title ">Edit Himpunan</h3></div>
                             <div class="card-body">             
 
                                 <div class="form-group{{ $errors->has('nama_himpunan') ? ' has-error' : '' }}">
                                     <div class="col-md-12">
                                         <label for="nama">Nama Himpunan</label>
                                     </div>
                                          
                                     <div class=" col-md-12">
                                         <div class="input-group"> 
                                             {{-- <div class="input-group-addon"><i style="color: #b82601" class="fa fa-male" aria-hidden="true"></i></div> --}}
                                             <input id="nama_himpunan" type="text" class="form-control" name="nama_himpunan" value="{{$data->nama_himpunan}}" required autofocus>
 
                                              @if ($errors->has('nama_himpunan'))
                                                  <span class="help-block">
                                                     <strong>{{ $errors->first('nama_himpunan') }}</strong>
                                                  </span>
                                             @endif
                                         </div>
                                         
                                     </div>
                                 </div>

                                 <div class="form-group{{ $errors->has('batasbawah') ? ' has-error' : '' }}">
                                        <div class="col-md-12">
                                            <label for="nama">Batas Bawah</label>
                                        </div>
                                             
                                        <div class=" col-md-12">
                                            <div class="input-group"> 
                                                {{-- <div class="input-group-addon"><i style="color: #b82601" class="fa fa-male" aria-hidden="true"></i></div> --}}
                                                <input id="batasbawah" type="text" class="form-control" name="batasbawah" value="{{$data->batasbawah}}"  required autofocus>
    
                                                 @if ($errors->has('batasbawah'))
                                                     <span class="help-block">
                                                        <strong>{{ $errors->first('batasbawah') }}</strong>
                                                     </span>
                                                @endif
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('batastengah_a') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <label for="nama">Batas Tengah 1</label>
                                            </div>
                                                 
                                            <div class=" col-md-12">
                                                <div class="input-group"> 
                                                    {{-- <div class="input-group-addon"><i style="color: #b82601" class="fa fa-male" aria-hidden="true"></i></div> --}}
                                                    <input id="batastengah_a" type="text" class="form-control" name="batastengah_a" value="{{$data->batastengah_a}}" >
        
                                                     @if ($errors->has('batastengah_a'))
                                                         <span class="help-block">
                                                            <strong>{{ $errors->first('batastengah_a') }}</strong>
                                                         </span>
                                                    @endif
                                                </div>
                                                
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('batastengah_b') ? ' has-error' : '' }}">
                                            <div class="col-md-12">
                                                <label for="nama">Batas Tengah 2</label>
                                            </div>
                                                    
                                            <div class=" col-md-12">
                                                <div class="input-group"> 
                                                    {{-- <div class="input-group-addon"><i style="color: #b82601" class="fa fa-male" aria-hidden="true"></i></div> --}}
                                                    <input id="batastengah_b" type="text" class="form-control" name="batastengah_b" value="{{$data->batastengah_b}}" >
        
                                                        @if ($errors->has('batastengah_b'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('batastengah_b') }}</strong>
                                                            </span>
                                                    @endif
                                                </div>
                                                
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('batasatas') ? ' has-error' : '' }}">
                                                <div class="col-md-12">
                                                    <label for="nama">Batas Atas</label>
                                                </div>
                                                     
                                                <div class=" col-md-12">
                                                    <div class="input-group"> 
                                                        {{-- <div class="input-group-addon"><i style="color: #b82601" class="fa fa-male" aria-hidden="true"></i></div> --}}
                                                        <input id="batasatas" type="text" class="form-control" name="batasatas" value="{{$data->batasatas}}"  required autofocus>
            
                                                         @if ($errors->has('batasatas'))
                                                             <span class="help-block">
                                                                <strong>{{ $errors->first('batasatas') }}</strong>
                                                             </span>
                                                        @endif
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                 <br>
                            
                             </div>
                     </div>
                 </div> 
 
                 </div>
 
                 <div class="form-group">
                     <div class="col-md-8 col-md-offset-10">
                         <button type="submit" class="btn btn-primary">
                             Submit
                          </button>
                     </div>                    
                 </div>      
         {!! Form::close() !!}
     </div>
@endsection