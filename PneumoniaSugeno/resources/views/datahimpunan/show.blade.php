@extends('layouts.dashboard')

@section('sidebar')
<!DOCTYPE html>

{{-- <div class="table-responsive" style="margin:0; width: 1050px"> --}}
    <table class="table"  id="users-table">
        <thead>
            <tr>   
                <th>id</th>
                <th>Nama_Himpunan</th>
                <th>Batas_Bawah</th>
                <th>Batas_Tengah_A</th>
                <th>Batas_Tengah_B</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php $i=1; ?>
            @foreach($data as $dt)
                <tr>
                <td><?= $i++ ?></td>
                <td>{{ $dt->nama_himpunan }}</td>
                <td>{{ $dt->batasbawah }}</td>
                <td>{{ $dt->batastengah_a }}</td>
                <td>{{ $dt->batastengah_b }}</td>
                <td>{{ $dt->batasatas }}</td>
                {{-- <td>
                    <form method="post" action="{{route('pasien.destroy', $dt->id_pasien)}}" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="DELETE">                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                        <input type="submit" class="btn btn-xs btn-danger" onclick="return confirm('yakin ingin menghapus ?');" value="delete"> 	
                    </form>
                </td> --}}
                <td>
                    <a class="btn btn-xs btn-primary" href="{{route('daftarvariabel.edit', $dt->id_himpunan)}}">edit</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
{{-- </div> --}}
    
@endsection
}