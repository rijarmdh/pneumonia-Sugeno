@extends('layouts.dashboard')

@section('sidebar')

<style type="text/css">
    th{
        font-size: 13px;
    }
    td{
        font-size: 13px;
    }
</style>

<br><br><br>
    <section style="margin-right: 5%;" class="content-header responsive">
          <h1>
            Halaman Detail Pasien
            
          </h1>
             <ul class="breadcrumb">
            <li><a href="{{route('pasien.index')}}"><i class="fa fa-dashboard"></i> Data Pasien</a></li>
            ><li class="active">Detail Pasien</li>
          </ul>
    </section>

    <br>
    <div class="container">
        {{-- <a href={{action('pdfController@getPDF', $data->id_gejala)}} class="btn btn-sm btn-primary">Print</a> --}}
        
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header with-border">
                    <h3 class="card-title">Detail Data Pasien</h3> 
                    </div>

                     {{ csrf_field() }}
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td class="text-muted">
                                        <b>id</b>    
                                        </td>

                                        <td>{{$data->Pasien->id_pasien}}</td>        
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                        <b>Nama</b>    
                                        </td>

                                        <td>{{$data->Pasien->nama}}</td>        
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                        <b>Tanggal Periksa</b>    
                                        </td>

                                        <td>{{Carbon\Carbon::parse($data->created_at)->format('d F, Y  \p\u\k\u\l\ \ h:i A') }}</td>        
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Suhu</b></td>
                                        <td>{{$data->suhu}}</td>
                                    </tr>   

                                    <tr>
                                        <td class="text-muted"><b>Nadi</b></td>

                                        <td>
                                            {{$data->nadi}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Pernafasan</b></td>

                                        <td>
                                            {{$data->pernafasan}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Usia</b></td>

                                        <td>
                                            {{$data->usia}}
                                        </td>
                                    </tr>                                   
                                </tbody>
                            </table>    
                        </div>
                </div> 


                <div class="card card-success">
                    <div class="card-header with-border">
                        <h3 class="card-title">Riwayat Penyakit Pasien</h3>
                    </div>
                        <div class="card-body">
                            <table class="table table-hover">
                                <tbody>
                                     <tr>
                                        <td class="text-muted"><b>Keganasan</b></td>

                                        <td>
                                        @if ($data->keganasan == 1)
                                            <?php echo "iya" ?>
                                        @else
                                             <?php echo "tidak" ?> 
                                        @endif

                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Penyakit Hati</b></td>

                                        <td>
                                            @if ($data->penyakithati == "1")
                                                <?php echo "iya" ?>
                                            @else
                                                <?php echo "tidak" ?>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Penyakit Jantung Kongestif</b></td>

                                        <td>
                                            @if ($data->jantung == "1")
                                                <?php echo "iya" ?>
                                            @else
                                                <?php echo "tidak" ?>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Penyakit Serebrovaskular</b></td>

                                        <td>
                                            @if ($data->serebrovaskular == "1")
                                                <?php echo "iya" ?>
                                            @else
                                                <?php echo "tidak" ?>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Penyakit Ginjal</b></td>

                                        <td>
                                            @if ($data->ginjal == "1")
                                                <?php echo "iya" ?>
                                            @else
                                                <?php echo "tidak" ?>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Penyakit Gangguan Kesadaran</b></td>

                                        <td>
                                            @if ($data->gangguan_kesadaran == "1")
                                                <?php echo "iya" ?>
                                            @else
                                                <?php echo "tidak" ?>
                                            @endif
                                        </td>
                                    </tr>

                                    



                                </tbody>
                            </table>
                        </div>
                </div>
            </div>

            <div class="col-md-6">
               <div class="card card-warning">
                   <div class="card-header with-border">
                       <h3 class="card-title">Hasil Laboratorium</h3>
                   </div>

                   <div class="card-body">
                       <table class="table table-hover">
                           <tbody>
                                <tr>
                                        <td class="text-muted"><b>PH</b></td>

                                        <td>
                                            {{$data->ph}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Bloode Urea Nitrogen</b></td>

                                        <td>
                                            {{$data->bun}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Natrium</b></td>

                                        <td>
                                            {{$data->natrium}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Glukosa</b></td>

                                        <td>
                                            {{$data->glukosa}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Hematokrit</b></td>

                                        <td>
                                            {{$data->hematokrit}}
                                        </td>
                                    </tr>

                                     <tr>
                                        <td class="text-muted"><b>Pao2</b></td>

                                        <td>
                                            {{$data->pao2}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Sistolik</b></td>

                                        <td>
                                            {{$data->sistolik}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted"><b>Efusi Pleura</b></td>

                                        <td>
                                        @if ($data->efusi_pleura == 1)
                                            <?php echo "ya" ?>
                                        @else
                                            <?php echo "tidak" ?>
                                        @endif
                                           
                                        </td>
                                    </tr>
                           </tbody>
                       </table>
                   </div>
               </div>

               <div class="card card-success">
                   <div class="card-header with-border">
                       <h3 class="card-title">
                           Hasil Pemeriksaan
                       </h3>
                   </div>

                   <div class="card-body">
                       <table class="table">
                           <tbody class="table table-hover">
                               <tr>
                                    <td class="text-muted"><b>Skor Pneumonia</b></td>

                                    <td>
                                      {{$data->nilaiz}}
                                    </td>
                            
                                </tr>

                                <tr>
                                    <td class="text-muted"><b>Kesimpulan Penyakit Pasien</b></td>

                                    <td>
                                        {{$data->pneumonia}}
                                    </td>
                                </tr>

                                 <tr>
                                    <td class="text-muted"><b>Saran Perawatan</b></td>

                                    <td>
                                        {{$data->solusi}}
                                    </td>
                                </tr>
                           </tbody>
                       </table>
                   </div>
               </div>

            </div>
        </div> 
    </div>
    
@endsection