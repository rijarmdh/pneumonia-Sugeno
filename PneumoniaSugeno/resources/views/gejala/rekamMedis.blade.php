@extends('layouts.dashboard')

@section('sidebar')
 
<style type="text/css">
	th {
		text-align: center;
		font-size: 13px;
	}

	td{
		text-align: center;
		font-size: 13px;
	}
</style>



<br><br><br>
	<section style="margin-right: 5%;" class="content-header responsive">
          <h1>
            Halaman Data Rekam Medis
            
          </h1>
             <ul class="breadcrumb">
            <li><a href="dashboardlte"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            ><li class="active">Tabel Rekam Medis</li>
          </ul>

           <div class="row">
				<div class="col-lg-12">
					{{ Form::open(['method'=>'GET', 'action'=>'gejalaController@riwayat_rekam_medis', 'class'=>'navbar-form navbar-right' ]) }}
								 	
				<input type="text" name="search" class="form-control col-lg-12" required>
			 	
			 	<span>
				 	<button type="submit" class="btn btn-primary" >
						<i class="fa fa-search"></i>	
					</button>
				 	
			 		<a  class="btn btn-primary" href="{{ action('gejalaController@riwayat_rekam_medis') }}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
				</span>

			 		{{ Form::close() }}	
				</div>
			</div>
    </section>

	<br>
  {{-- <div class="container">
   --}}
   <div class="row">
  		<div class="col-lg-12">
  			
				<div class="card card-danger">
					<div class="card-header with-border">
					<h3 class="card-title">Riwayat Rekam Medis Pasien</h3>
					</div>
					<div class="card-body table-responsive">
						<div class="table-responsive">
							<table class="table  table-striped table-hover">
								<thead>
									<tr>
										<th>id</th>
										<th>Nama</th>
										<th><b>Alamat</b></th>
										<th><b>Tempat Lahir</b></th>
										<th><b>Tanggal Lahir</b></i></th>
										<th>Jenis Kelamin</th>
										
									</tr>
								</thead>

								<tbody>

									@foreach($rekammedis as $rekmed)
										<tr>

											<td>{{$rekmed->id_pasien}}</td>
											<td>{{$rekmed->nama}}</td>
											<td>{{$rekmed->alamat}}</td>
											<td>{{$rekmed->tempat_lahir}}</td>
											<td>{{$rekmed->tanggal_lahir}}</td>
											<td>{{$rekmed->jenis_kelamin}}</td>
											
											
											<td>

										{{-- 	<form method="post" action="{{route('datagejala.destroy', $rekmed->id_pasien)}}" accept-charset="UTF-8">
												<input type="hidden" name="_method" value="DELETE">
												
												<input type="hidden" name="_token" value="{{ csrf_token() }}" >

												<input type="submit" class="btn btn-xs btn-danger" onclick="return confirm('yakin ingin menghapus ?');" value="delete"> 
												
											</form> --}}
													
											</td>

											<td>
												<a class="btn btn-xs btn-primary" href="{{action('gejalaController@rekam_medis_per_pasien', $rekmed->id_pasien)}}">Detail</a>
											</td>
										
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>		
			</div>
  		</div>
  </div>

@endsection