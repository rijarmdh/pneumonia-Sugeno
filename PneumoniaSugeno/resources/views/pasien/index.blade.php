@extends('layouts.dashboard')

@section('sidebar')
<!DOCTYPE html>

{{-- <div class="table-responsive" style="margin:0; width: 1050px"> --}}
    <table class="table"  id="users-table">
        <thead>
            <tr>   
                <th>Nama</th>
                <th>Alamat</th>
                <th>Nomor_Telepon</th>
                <th>Tempat_Lahir</th>
                <th>Tanggal_Lahir</th>
                <th>Jenis_Kelamin</th>
                <th>Status</th>
                <th></th>
                <th>Action</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $dt)
                <tr>
                <td>{{ $dt->nama }}</td>
                <td>{{ $dt->alamat }}</td>
                <td>{{ $dt->no_telepon }}</td>
                <td>{{ $dt->tempat_lahir }}</td>
                <td>{{ $dt->tanggal_lahir }}</td>
                <td>{{ $dt->jenis_kelamin }}</td>
                <td>{{ $dt->status_perkawinan }}</td>
                <td>
                    <form method="post" action="{{route('pasien.destroy', $dt->id_pasien)}}" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="DELETE">                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                        <input type="submit" class="btn btn-xs btn-danger" onclick="return confirm('yakin ingin menghapus ?');" value="delete"> 	
                    </form>
                </td>
                <td>
                    <a class="btn btn-xs btn-primary" href="{{route('pasien.edit', $dt->id_pasien)}}">edit</a>
                </td>
                <td>
                    <a href="{{route('pasien.show', $dt->id_pasien)}}" class="btn btn-xs btn-warning">detail</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
{{-- </div> --}}
    
@endsection

{{-- @push('scripts')
<script>
$(document).ready( function () {
    $('#users-table').DataTable({
        processing:true,
        serverSide: true,
        ajax :'{{ route("pasien.data") }}',
        columns:[
            {data: 'nama', name:'nama'},
            {data: 'nama_panggilan'},
            {data: 'alamat'},
            {data: 'no_telepon'},
            {data: 'tempat_lahir'},
            {data: 'tanggal_lahir'},
            {data: 'jenis_kelamin'},
            {data: 'status_perkawinan'},
            {data: 'kewarganegaraan'},
            {data: 'agama'},
            {data: 'pekerjaan'},
            {data: 'email'},
            {data: 'pendidikan'},
            {data: 'nama_kerabat'},
            {data: 'hubungan'},
            {data: 'nomor_telepon'},
        ],
        "scrollX": true
    });    
} );

</script>
@endpush --}}