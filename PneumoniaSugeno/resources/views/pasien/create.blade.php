@extends('layouts.dashboard')

@section('sidebar')
<!DOCTYPE html>

<form method="POST" action="{{ route('pasien.store') }}">
    @csrf
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header"><strong>Tambah Pasien</strong><small> Form</small></div>
            <div class="card-body card-block">
                <div class="form-group">
                    <label for="nama" >{{ __('Nama') }}</label>
                        <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{ old('nama') }}" required autofocus>

                        @if ($errors->has('nama'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('nama') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group">
                        <label for="nama_panggilan" >{{ __('Nama Panggilan') }}</label>
                            <input id="nama_panggilan" type="text" class="form-control{{ $errors->has('nama_panggilan') ? ' is-invalid' : '' }}" name="nama_panggilan" value="{{ old('nama_panggilan') }}" required autofocus>

                            @if ($errors->has('nama_panggilan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('nama_panggilan') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group">
                        <label for="alamat" >{{ __('Alamat') }}</label>
                            <input id="alamat" type="text" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" name="alamat" value="{{ old('alamat') }}" required autofocus>

                            @if ($errors->has('alamat'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('alamat') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group">
                        <label for="no_telepon" >{{ __('Telepon') }}</label>
                            <input id="no_telepon" type="text" class="form-control{{ $errors->has('no_telepon') ? ' is-invalid' : '' }}" name="no_telepon" value="{{ old('no_telepon') }}" required autofocus>

                            @if ($errors->has('no_telepon'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('no_telepon') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group">
                        <label for="tempat_lahir" >{{ __('Tempat Lahir') }}</label>
                            <input id="tempat_lahir" type="text" class="form-control{{ $errors->has('tempat_lahir') ? ' is-invalid' : '' }}" name="tempat_lahir" value="{{ old('tempat_lahir') }}" required autofocus>

                            @if ($errors->has('tempat_lahir'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('tempat_lahir') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group">
                        <label for="tanggal_lahir" >{{ __('Tanggal Lahir') }}</label>
                            <input id="datepicker" type="text" class="form-control{{ $errors->has('tanggal_lahir') ? ' is-invalid' : '' }}" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" required autofocus>

                            @if ($errors->has('tanggal_lahir'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group">
                        <label for="jenis_kelamin" >{{ __('Jenis Kelamin') }}</label>
                            {{-- <input id="jenis_kelamin" type="text" class="form-control{{ $errors->has('jenis_kelamin') ? ' is-invalid' : '' }}" name="nama" value="{{ old('nama') }}" required autofocus> --}}
                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control{{ $errors->has('jenis_kelamin') ? ' is-invalid' : '' }}">
                                <option value="laki">laki-laki</option>
                                <option value="perempuan">perempuan</option>
                            </select>
                            @if ($errors->has('jenis_kelamin'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('jenis_kelamin') }}</strong>
                                </span>
                            @endif
                </div>

                <div class="form-group">
                        <label for="email" >{{ __('Email') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4>Data Tambahan Pasien</h4>
            </div>

            <div class="card-body">
                    <div class="form-group">
                        <label for="status_perkawinan" >{{ __('Status Perkawinan') }}</label>
                            {{-- <input id="jenis_kelamin" type="text" class="form-control{{ $errors->has('jenis_kelamin') ? ' is-invalid' : '' }}" name="nama" value="{{ old('nama') }}" required autofocus> --}}
                            <select name="status_perkawinan" id="status_perkawinan" class="form-control{{ $errors->has('status_perkawinan') ? ' is-invalid' : '' }}">
                                <option value="lajang">lajang</option>
                                <option value="menikah">menikah</option>
                            </select>
                            @if ($errors->has('Status_perkawinan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('status_perkawinan') }}</strong>
                                </span>
                            @endif
                    </div>

                    <div class="form-group">
                        <label for="kewarganegaraan" >{{ __('Kewarganegaraan') }}</label>
                            <input id="kewarganegaraan" type="text" class="form-control{{ $errors->has('kewarganegaraan') ? ' is-invalid' : '' }}" name="kewarganegaraan" value="{{ old('kewarganegaraan') }}" required autofocus>

                            @if ($errors->has('kewarganegaraan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('kewarganegaraan') }}</strong>
                                </span>
                            @endif
                    </div>

                    <div class="form-group">
                        <label for="agama" >{{ __('Agama') }}</label>
                            <input id="agama" type="text" class="form-control{{ $errors->has('agama') ? ' is-invalid' : '' }}" name="agama" value="{{ old('agama') }}" required autofocus>

                            @if ($errors->has('agama'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('agama') }}</strong>
                                </span>
                            @endif
                    </div>

                    <div class="form-group">
                        <label for="pekerjaan" >{{ __('Pekerjaan') }}</label>
                            <input id="pekerjaan" type="text" class="form-control{{ $errors->has('pekerjaan') ? ' is-invalid' : '' }}" name="pekerjaan" value="{{ old('pekerjaan') }}" required autofocus>

                            @if ($errors->has('pekerjaan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('pekerjaan') }}</strong>
                                </span>
                            @endif
                    </div>

                    <div class="form-group">
                        <label for="pendidikan" >{{ __('Pendidikan') }}</label>
                            <input id="pendidikan" type="text" class="form-control{{ $errors->has('pendidikan') ? ' is-invalid' : '' }}" name="pendidikan" value="{{ old('pendidikan') }}" required autofocus>

                            @if ($errors->has('pendidikan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('pendidikan') }}</strong>
                                </span>
                            @endif
                    </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4>Hubungan Kerabat</h4>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="nama_kerabat" >{{ __('Nama Kerabat') }}</label>
                        <input id="nama_kerabat" type="text" class="form-control{{ $errors->has('nama_kerabat') ? ' is-invalid' : '' }}" name="nama_kerabat" value="{{ old('nama_kerabat') }}" required autofocus>

                        @if ($errors->has('nama_kerabat'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('nama_kerabat') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group">
                    <label for="hubungan" >{{ __('Hubungan') }}</label>
                        <input id="hubungan" type="text" class="form-control{{ $errors->has('hubungan') ? ' is-invalid' : '' }}" name="hubungan" value="{{ old('hubungan') }}" required autofocus>

                        @if ($errors->has('hubungan'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('hubungan') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group">
                    <label for="nomor_telepon" >{{ __('Nomor Telepon') }}</label>
                        <input id="nomor_telepon" type="text" class="form-control{{ $errors->has('nomor_telepon') ? ' is-invalid' : '' }}" name="nomor_telepon" value="{{ old('nomor_telepon') }}" required autofocus>

                        @if ($errors->has('nomor_telepon'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('nomor_telepon') }}</strong>
                            </span>
                        @endif
                </div>
            </div>
            <div class="form-group">
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>
                    </div>
                </div>
        </div>
    </div>

</form>
@endsection