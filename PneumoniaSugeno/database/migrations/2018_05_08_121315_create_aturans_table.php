<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAturansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aturans', function (Blueprint $table) {
            $table->increments('id_aturan')->unsigned();
            $table->string('nama_aturan', 10);
            $table->string('suhu', 25);
            $table->string('nadi', 25);
            $table->string('pernafasan', 25);
            $table->string('usia', 25);
            $table->string('pao2', 25);
            $table->string('sistolik', 25);
            $table->string('ph', 25);
            $table->string('bun', 25);
            $table->string('natrium', 25);
            $table->string('glukosa', 25);
            $table->string('hematokrit', 25);
            $table->string('efusi_pleura', 25);
            $table->string('keganasan', 25);
            $table->string('penyakit_hati', 25);
            $table->string('jantung', 25);
            $table->string('serebrovaskular', 25);
            $table->string('ginjal', 25);
            $table->string('gangguan_kesadaran', 25);
            $table->string('pneumonia', 25);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aturans');
    }
}
