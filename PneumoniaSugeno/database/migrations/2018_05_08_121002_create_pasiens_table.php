<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table) {
            $table->increments('id_pasien')->unsigned();
            $table->string('nama', 50)->nullable();
            $table->string('nama_panggilan', 20)->nullable();
            $table->text('alamat')->nullable();
            $table->string('no_telepon', 30)->unique();
            $table->string('tempat_lahir', 30)->nullable();
            $table->string('tanggal_lahir', 30)->nullable();
            $table->enum('jenis_kelamin', ['laki', 'perempuan']);
            $table->enum('status_perkawinan', ['lajang', 'menikah']);
            $table->string('kewarganegaraan', 20)->nullable();
            $table->string('agama', 20)->nullable();
            $table->string('pekerjaan')->nullable();
            $table->string('email', 25)->nullable();
            $table->string('pendidikan', 20)->nullable();
            $table->string('nama_kerabat', 50)->nullable();
            $table->string('hubungan', 40)->nullable();
            $table->string('nomor_telepon', 20)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasiens');
    }
}
