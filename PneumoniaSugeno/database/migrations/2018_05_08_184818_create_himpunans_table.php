<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHimpunansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('himpunans', function (Blueprint $table) {
            $table->increments('id_himpunan')->unsigned();
            $table->integer('id_variable')->length(10)->unsigned();
            $table->string('nama_himpunan', 50);
            $table->double('batasbawah');
            $table->double('batastengah_a')->nullable();
            $table->double('batastengah_b')->nullable();
            $table->double('batasatas');
            $table->timestamps();
        
            $table->foreign('id_variable')
            ->references('id_variable')
            ->on('daftar__variabels')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('himpunans');
    }
}
