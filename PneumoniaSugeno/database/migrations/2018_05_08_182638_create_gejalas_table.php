<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGejalasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gejalas', function (Blueprint $table) {
            $table->increments('id_gejala')->unsigned();
            $table->integer('id_pasien')->length(10)->unsigned();
            $table->double('suhu');
            $table->double('nadi');
            $table->double('pernafasan');
            $table->string('usia', 7);
            $table->double('pao2');
            $table->double('sistolik');
            $table->double('ph');
            $table->double('bun');
            $table->double('natrium');
            $table->double('glukosa');
            $table->double('hematokrit');
            $table->enum('efusi_pleura', ['ya', 'tidak']);
            $table->enum('keganasan', ['ya', 'tidak']);
            $table->enum('penyakit_hati', ['ya', 'tidak']);
            $table->enum('jantung', ['ya', 'tidak']);
            $table->enum('serebrovaskular', ['ya', 'tidak']);
            $table->enum('ginjal', ['ya', 'tidak']);
            $table->enum('gangguan_kesadaran', ['ya', 'tidak']);
            $table->integer('nilai_z')->length(30);
            $table->string('pneumonia',25);
            $table->integer('id_solusi')->length(10)->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id_pasien')
                    ->references('id_pasien')
                    ->on('pasiens')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('id_solusi')
                    ->references('id_solusi')
                    ->on('solusis')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gejalas');
    }
}
